#ifndef DUNE_VISU_PROBDENSITY_HH
#define DUNE_VISU_PROBDENSITY_HH

template<typename Vector, typename Range, unsigned int dim>
class ProbDensity
{
  private:

    Vector meanVector, radius;
    Range factor;

    mutable Vector diff;

  public:

    ProbDensity()
      : factor(std::pow(2.*3.14159,-(dim/2.)))
    {
      for (unsigned int i = 0; i < dim; i++)
        radius[i] = 1.;
    }

    void setMean(const Vector& meanVector_)
    {
      meanVector = meanVector_;
    }

    void setRadius(const Vector& radius_)
    {
      radius = radius_;
      factor = std::pow(2.*3.14159*radius*radius,-(dim/2.));
    }

    Range operator()(const Vector& point) const
    {
      diff = point - meanVector;
      for (unsigned int i = 0; i < dim; i++)
        diff[i] /= radius[i];
      return factor * std::exp(-0.5 * (diff*diff));
    }

};

#endif // DUNE_VISU_PROBDENSITY_HH
