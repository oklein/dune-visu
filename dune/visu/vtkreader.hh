#ifndef DUNE_VISU_VTKREADER
#define DUNE_VISU_VTKREADER

#include <vtkSmartPointer.h>
#include <vtkXMLReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLHyperOctreeReader.h>
#include <vtkXMLCompositeDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLImageDataReader.h>
#include <vtkDataSetReader.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkRectilinearGrid.h>
#include <vtkHyperOctree.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkStructuredGrid.h>
#include <vtkCell.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFieldData.h>
#include <vtkCellTypes.h>

#include <vtkCellLocator.h>
#include <vtkCellTreeLocator.h>

#include <vtkGenericCell.h>

#include <dune/visu/vector.hh>

class VTKReader
{
  private:

    mutable vtkSmartPointer<vtkDataSet> dataSet;
    mutable vtkSmartPointer<vtkCellTreeLocator> cellTreeLocator;

    mutable vtkSmartPointer<vtkGenericCell> genericCell;
    mutable vtkIdType cellId;
    mutable double bounds[6];
    mutable double coords[3];
    mutable double pcoords[3];
    mutable double weights[10];
    mutable double tuple[10];

    const std::string fileName;
    const std::array<bool,3> periodic;
    int arrayId;
    mutable vtkDataArray* dataArray;
    mutable int numberOfComponents;

    mutable double closestPoint[3];
    mutable int subId;
    mutable double dist2;

    const double cutoffValue;

    mutable bool valid;

  public:

    VTKReader(const std::string& fileName_, const std::array<bool,3>& periodic_ = {0,0,0}, int arrayId_ = 0, double cutoffValue_ = std::numeric_limits<double>::max())
      : fileName(fileName_), periodic(periodic_), arrayId(arrayId_), cutoffValue(cutoffValue_), valid(false)
      {
        for (int i = 0; i < 3; i++)
          coords[i] = 0.;
      }

    void init() const
    {
      std::string::size_type idx = fileName.rfind('.');

      std::string extension;
      if(idx != std::string::npos)
        extension = fileName.substr(idx);
      else
        DUNE_THROW(Dune::Exception,"VTK filename is missing extension");

      // Dispatch based on file extension
      if (extension == ".vtu")
        dataSet = ReadXMLFile<vtkXMLUnstructuredGridReader>(fileName);
      else if (extension == ".pvtu")
        dataSet = ReadXMLFile<vtkXMLPUnstructuredGridReader>(fileName);
      else if (extension == ".vtp")
        dataSet = ReadXMLFile<vtkXMLPolyDataReader>(fileName);
      else if (extension == ".vts")
        dataSet = ReadXMLFile<vtkXMLStructuredGridReader>(fileName);
      else if (extension == ".vtr")
        dataSet = ReadXMLFile<vtkXMLRectilinearGridReader>(fileName);
      else if (extension == ".vti")
        dataSet = ReadXMLFile<vtkXMLImageDataReader>(fileName);
      else if (extension == ".vto")
        dataSet = ReadXMLFile<vtkXMLHyperOctreeReader>(fileName);
      else if (extension == ".vtk")
        dataSet = ReadXMLFile<vtkDataSetReader>(fileName);
      else
        DUNE_THROW(Dune::Exception,"Unknown VTK extension " + extension);

      dataSet->GetBounds(bounds);

      cellTreeLocator = vtkSmartPointer<vtkCellTreeLocator>::New();
      cellTreeLocator->SetDataSet(dataSet);
      cellTreeLocator->BuildLocator();

      genericCell = vtkSmartPointer<vtkGenericCell>::New();

      dataArray          = dataSet->GetPointData()->GetArray(arrayId);
      numberOfComponents = dataArray->GetNumberOfComponents();

      valid = true;
    }

    void setArrayId(int arrayId_)
    {
      arrayId = arrayId_;

      if (valid)
      {
        dataArray          = dataSet->GetPointData()->GetArray(arrayId);
        numberOfComponents = dataArray->GetNumberOfComponents();
      }
    }

    int getArrayId() const
    {
      if (!valid)
        init();

      return arrayId;
    }

    int getNumberOfComponents() const
    {
      if (!valid)
        init();

      return numberOfComponents;
    }

    template<typename Vector, unsigned int dim>
      bool eval(const Vector& location, Vector& data) const
      {
        if (!valid)
          init();

        /// @todo
        int timesLon = 0, timesLat = 0;
        Vector vectorCopy = location;
        normalizeCoords<Vector,dim>(vectorCopy);
        vectorCopy.normalize(timesLon,timesLat);

        /// @todo
        for (unsigned int i = 0; i < dim; i++)
          coords[i] = vectorCopy[i];

        if (!(genericCell->EvaluatePosition(coords,closestPoint,subId,pcoords,dist2,weights)==1))
          cellId = cellTreeLocator->FindCell(coords,0,genericCell,pcoords,weights);

        if (cellId >= 0)
        {
          for (int i = 0; i < numberOfComponents; i++)
            data[i] = 0.;

          for (int i = 0; i < genericCell->GetNumberOfPoints(); i++)
          {
            dataArray->GetTuple(genericCell->GetPointId(i),tuple);

            for (int j = 0; j < numberOfComponents; j++)
              data[j] += weights[i] * tuple[j];
          }

          data.switchSign(timesLon,timesLat);

          for (int i = 0; i < numberOfComponents; i++)
          {
            if (data[i] < -cutoffValue)
              data[i] = -cutoffValue;
            if (data[i] > cutoffValue)
              data[i] = cutoffValue;
          }

          return true;
        }

        return false;
      }

    template<typename Vector, typename Range, unsigned int dim>
      bool eval(const Vector& location, const Range& time, Vector& data) const
      {
        return eval<Vector,dim>(location,data);
      }

    void report() const
    {
      if (!valid)
        init();

      int numberOfCells = dataSet->GetNumberOfCells();
      int numberOfPoints = dataSet->GetNumberOfPoints();

      // Generate a report
      std::cout << "------------------------" << std::endl;
      std::cout << "dataset contains a " << std::endl
        << dataSet->GetClassName()
        <<  " that has " << numberOfCells << " cells"
        << " and " << numberOfPoints << " points." << std::endl;
      typedef std::map<int,int> CellContainer;
      CellContainer cellMap;
      for (int i = 0; i < numberOfCells; i++)
      {
        cellMap[dataSet->GetCellType(i)]++;
      }

      CellContainer::const_iterator it = cellMap.begin();
      while (it != cellMap.end())
      {
        std::cout << "\tCell type "
          << vtkCellTypes::GetClassNameFromTypeId(it->first)
          << " occurs " << it->second << " times." << std::endl;
        ++it;
      }

      // Now check for point data
      vtkPointData* pd = dataSet->GetPointData();
      if (pd)
      {
        std::cout << " contains point data with "
          << pd->GetNumberOfArrays()
          << " arrays." << std::endl;
        for (int i = 0; i < pd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i
            << " is named "
            << (pd->GetArrayName(i) ? pd->GetArrayName(i) : "NULL")
            << std::endl;

          int numberOfTuples     = pd->GetArray(i)->GetNumberOfTuples();
          int numberOfComponents = pd->GetArray(i)->GetNumberOfComponents();
          std::cout << "\tArray " << i
            << " has " << numberOfTuples << " tuples with "
            << numberOfComponents << " components" << std::endl;
        }
      }
      // Now check for cell data
      vtkCellData* cd = dataSet->GetCellData();
      if (cd)
      {
        std::cout << " contains cell data with "
          << cd->GetNumberOfArrays()
          << " arrays." << std::endl;
        for (int i = 0; i < cd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i
            << " is named "
            << (cd->GetArrayName(i) ? cd->GetArrayName(i) : "NULL")
            << std::endl;

          int numberOfTuples     = cd->GetArray(i)->GetNumberOfTuples();
          int numberOfComponents = cd->GetArray(i)->GetNumberOfComponents();
          std::cout << "\tArray " << i
            << " has " << numberOfTuples << " tuples with "
            << numberOfComponents << " components" << std::endl;
        }
      }
      // Now check for field data
      if (dataSet->GetFieldData())
      {
        std::cout << " contains field data with "
          << dataSet->GetFieldData()->GetNumberOfArrays()
          << " arrays." << std::endl;
        for (int i = 0; i < dataSet->GetFieldData()->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i
            << " is named " << dataSet->GetFieldData()->GetArray(i)->GetName()
            << std::endl;
        }
      }
    }

  private:

    template<class ReaderType>
      vtkSmartPointer<vtkDataSet> ReadXMLFile(const std::string& fileName) const
      {
        vtkSmartPointer<ReaderType> reader(vtkSmartPointer<ReaderType>::New());
        reader->SetFileName(fileName.c_str());
        reader->Update();
        reader->GetOutput()->Register(reader);
        return reader->GetOutput();
      }

    template<typename Vector, unsigned int dim>
      void normalizeCoords(Vector& location) const
      {
        for (unsigned int i = 0; i < dim; i++)
          if (periodic[i])
          {
            double eps = 1e-6;
            while (location[i] > bounds[2*i+1])
              location[i] -= bounds[2*i+1] - bounds[2*i] - eps;
            while (location[i] < bounds[2*i])
              location[i] += bounds[2*i+1] - bounds[2*i] - eps;
          }
      }

};

#endif // DUNE_VISU_VTKREADER
