#ifndef DUNE_VISU_ENSEMBLEFROMTIMEREADER
#define DUNE_VISU_ENSEMBLEFROMTIMEREADER

#include<fstream>
#include<sstream>
#include<string>
#include<regex>

#include<list>

#include<dune/visu/pvdreader.hh>
#include<dune/visu/vector.hh>

class EnsembleFromTimeReader
{
  private:

    std::list<std::pair<double,VTKReader> > vtkReaders;
    double firstTime, lastTime;
    mutable double lowerTime, upperTime;
    mutable std::list<std::pair<double,VTKReader> >::const_iterator lowerData, upperData;

  public:

    EnsembleFromTimeReader(const std::string& fileName, const std::array<bool,3>& periodic = {0,0,0}, int arrayId_ = 0, double cutoffValue_ = std::numeric_limits<double>::max())
      : firstTime(std::numeric_limits<double>::max()),
      lastTime(-std::numeric_limits<double>::max()),
      lowerTime(std::numeric_limits<double>::max()),
      upperTime(-std::numeric_limits<double>::max())
      {
        std::ifstream infile(fileName);

        std::string line;
        while (std::getline(infile, line))
        {
          std::regex  timeRegex("timestep=\"([^\"]*)\"");
          std::smatch timeMatch;
          std::regex  vtkRegex("file=\"([^\"]*)\"");
          std::smatch vtkMatch;

          // work around regex bug in GCC compiler
          const std::string lineCopy = line;
          bool timeFound = std::regex_search(lineCopy.begin(), lineCopy.end(), timeMatch, timeRegex);
          bool vtkFound  = std::regex_search(lineCopy.begin(), lineCopy.end(), vtkMatch,  vtkRegex);

          if (timeFound && vtkFound)
          {
            std::stringstream s;
            s << timeMatch[1];
            double time;
            s >> time;

            if (time > lastTime)
            {
              vtkReaders.push_back(std::pair<double,VTKReader>(time,VTKReader(vtkMatch[1],periodic,arrayId_,cutoffValue_)));
              lastTime = time;
              if (time < firstTime)
                firstTime = time;
            }
            else if (time < firstTime)
            {
              vtkReaders.push_front(std::pair<double,VTKReader>(time,VTKReader(vtkMatch[1],periodic,arrayId_,cutoffValue_)));
              firstTime = time;
            }
            else
            {
              typename std::list<std::pair<double,VTKReader> >::iterator it = vtkReaders.begin();
              while (it != vtkReaders.end() && it->first < time)
                ++it;
              vtkReaders.insert(it, std::pair<double,VTKReader>(time,VTKReader(vtkMatch[1],periodic,arrayId_,cutoffValue_)));
            }
          }
        }

        infile.close();
        if (vtkReaders.empty())
          DUNE_THROW(Dune::Exception,"PVDReader is empty, no VTK files found");

        lowerTime = firstTime;
        upperTime = firstTime;
        lowerData = vtkReaders.begin();
        upperData = vtkReaders.begin();
      }

    void setArrayId(int arrayId_)
    {
      std::list<std::pair<double,VTKReader> >::iterator it;
      for (it = vtkReaders.begin(); it != vtkReaders.end(); ++it)
        it->second.setArrayId(arrayId_);
    }

    int getArrayId() const
    {
      int arrayId = vtkReaders.begin()->second.getArrayId();
      std::list<std::pair<double,VTKReader> >::const_iterator it;
      for (it = vtkReaders.begin(); it != vtkReaders.end(); ++it)
        if (it->second.getArrayId() != arrayId)
          DUNE_THROW(Dune::Exception,"inconsistent arrayId in PVDReader");

      return arrayId;
    }

    int getNumberOfComponents() const
    {
      int numberOfComponents = vtkReaders.begin()->second.getNumberOfComponents();
      std::list<std::pair<double,VTKReader> >::const_iterator it;
      for (it = vtkReaders.begin(); it != vtkReaders.end(); ++it)
        if (it->second.getNumberOfComponents() != numberOfComponents)
          DUNE_THROW(Dune::Exception,"inconsistent numberOfComponents in PVDReader");

      return numberOfComponents;
    }

    template<typename Vector, typename Range, unsigned int dim>
      bool eval(const Vector& location, const Range& time, Vector& data) const
      {
        const Range shiftedTime = time - location[dim-1];

        if (!updateIterators(shiftedTime))
          return false;

        if (std::abs(shiftedTime - lowerTime) < 1e-6)
          return lowerData->second.eval<Vector,dim>(location,data);
        else if (std::abs(shiftedTime - upperTime) < 1e-6)
          return upperData->second.eval<Vector,dim>(location,data);
        else
        {
          Vector lowerVector, upperVector;
          bool lowerValid, upperValid;
          lowerValid = lowerData->second.eval<Vector,dim>(location,lowerVector);
          upperValid = upperData->second.eval<Vector,dim>(location,upperVector);

          if (lowerValid && upperValid)
          {
            data = lowerVector;
            data *= (upperTime-shiftedTime)/(upperTime-lowerTime);
            data.axpy((shiftedTime-lowerTime)/(upperTime-lowerTime),upperVector);
            return true;
          }
          else
            return false;
        }
      }

    void report() const
    {
      std::list<std::pair<double,VTKReader> >::const_iterator it;
      for (it = vtkReaders.begin(); it != vtkReaders.end(); ++it)
        it->second.report();
    }

  private:

    bool updateIterators(double time) const
    {
      if (time - firstTime < -1e-6 || time - lastTime > 1e-6)
        return false;

      if (std::abs(time - firstTime) < 1e-6)
      {
        lowerTime = firstTime;
        upperTime = firstTime;
        lowerData = vtkReaders.begin();
        upperData = vtkReaders.begin();
      }
      else if (!(time - upperTime < 1e-6) || !(lowerTime - time < 1e-6))
      {
        while (time - upperTime < -1e-6)
        {
          if (upperData == vtkReaders.begin())
            break;
          upperData--;
          upperTime = upperData->first;
        }
        while (time - upperTime > 1e-6)
        {
          upperData++;
          if (upperData == vtkReaders.end())
            return false;
          upperTime = upperData->first;
        }

        lowerData = upperData;
        lowerTime = lowerData->first;
        if (lowerTime - time > 1e-6)
        {
          if (lowerData == vtkReaders.begin())
            return false;
          lowerData--;
          lowerTime = lowerData->first;
        }
      }

      return true;
    }

};

#endif // DUNE_VISU_ENSEMBLEFROMTIMEREADER
