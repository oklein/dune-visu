#ifndef DUNE_VISU_HH
#define DUNE_VISU_HH

#include<random>

#include<dune/common/parametertree.hh>
#include<dune/common/parametertreeparser.hh>

#include<dune/visu/vtkreader.hh>
#include<dune/visu/pvdreader.hh>
#include<dune/visu/ensemblepvdreader.hh>
#include<dune/visu/ensemblefromtimereader.hh>
#include<dune/visu/vector.hh>
#include<dune/visu/matrix.hh>
#include<dune/visu/tensor3.hh>
#include<dune/visu/pointset.hh>
#include<dune/visu/vectorfield.hh>
#include<dune/visu/flowmap.hh>
#include<dune/visu/probdensity.hh>
#include<dune/visu/statistics.hh>
#include<dune/visu/ftle.hh>
#include<dune/visu/vtkwriter.hh>

  template<template<typename,unsigned int> typename Vector, typename FlowField, unsigned int dim>
void computeFlowMap(const Dune::ParameterTree& config)
{
  const std::string        inputName      = config.get<std::string>         ("general.inputName");
  const std::array<bool,3> periodic       = config.get<std::array<bool,3> > ("general.periodic");
  const unsigned int       inputArrayId   = config.get<unsigned int>        ("general.inputArrayId");
  const std::string        outputName     = config.get<std::string>         ("general.outputName");
  const double             cutoffValue    = config.get<double>              ("general.cutoffValue");

  std::cout << "reading flow field" << std::endl;
  Dune::Timer timerFlowField(false);
  timerFlowField.start();
  FlowField flowField(inputName,periodic,inputArrayId,cutoffValue);
  timerFlowField.stop();
  std::cout << "Time for input: " << timerFlowField.elapsed() << std::endl;

  const Vector<double,dim> center              = config.get<std::array<double,dim> >      ("general.center");
  const Vector<double,dim> extent              = config.get<std::array<double,dim> >      ("general.extent");
  const std::array<unsigned int,dim> intervals = config.get<std::array<unsigned int,dim> >("general.intervals");
  PointGrid<Vector<double,dim>,double,dim> pointGrid(center,extent,intervals);

  const double startTime      = config.get<double>("general.startTime");
  const double timestep       = config.get<double>("general.timestep");
  const double timeResolution = config.get<double>("general.timeResolution");

  const unsigned int dataPartNumber = config.get<unsigned int>("general.dataPartNumber",0);
  const unsigned int dataParts      = config.get<unsigned int>("general.dataParts",1);

  WeightedFlowMap<Vector,FlowField,double,dim> flowMap(flowField,pointGrid,startTime,timeResolution,dataPartNumber,dataParts);

  Dune::Timer timerFlowMap(false);
  timerFlowMap.start();
  std::cout << "computing flow map" << std::endl;
  flowMap.setPostTime(startTime + timestep);
  timerFlowMap.stop();
  std::cout << "Time for flow map: " << timerFlowMap.elapsed() << std::endl;

  const PointSet<Vector<double,dim>,dim>& postSet  = flowMap.post();
  const unsigned int sliceSize = pointGrid.size()/dataParts+((pointGrid.size()%dataParts==0)?0:1);
  std::cout << "pointSet.size " << pointGrid.size() << " " << postSet.size() << " sliceSize " << sliceSize << std::endl;
  unsigned int invalid = 0;
  for (unsigned int i = dataPartNumber*sliceSize; i < (dataPartNumber+1)*sliceSize && i < pointGrid.size(); i++)
    if (!(postSet[i].isValid()))
      invalid++;
  std::cout << "FlowMap: invalid points " << invalid << " / " << pointGrid.size() << " (" << (double)invalid/(double)pointGrid.size()*100. << "%)" << std::endl;

  {
    Dune::Timer timerVector(false);
    timerVector.start();
    std::vector<Vector<double,dim> > postSetVector;
    for (unsigned int i = dataPartNumber*sliceSize; i < (dataPartNumber+1)*sliceSize && i < pointGrid.size(); i++)
    {
      postSetVector.push_back(postSet[i]);
    }
    timerVector.stop();
    std::cout << "Time for flow map vector: " << timerVector.elapsed() << std::endl;
    VTKWriter vtkWriter;
    Dune::Timer timerOutput(false);
    timerOutput.start();
    std::cout << "writing flow map" << std::endl;
    vtkWriter.writeVectorData(outputName + "_flowMap.vtk","flowMap",pointGrid,postSetVector,dataPartNumber,dataParts);
    std::cout << "Time for output: " << timerOutput.elapsed() << std::endl;
  }
  {
    Dune::Timer timerValid(false);
    timerValid.start();
    std::vector<double> validVector;
    for (unsigned int i = dataPartNumber*sliceSize; i < (dataPartNumber+1)*sliceSize && i < pointGrid.size(); i++)
    {
      if (postSet[i].isValid())
        validVector.push_back(1.);
      else
        validVector.push_back(0.);
    }
    timerValid.stop();
    std::cout << "Time for validity check: " << timerValid.elapsed() << std::endl;
    VTKWriter vtkWriter;
    Dune::Timer timerOutput(false);
    timerOutput.start();
    std::cout << "writing validity" << std::endl;
    vtkWriter.writeScalarData(outputName + "_valid.vtk",  "valid",  pointGrid,validVector,dataPartNumber,dataParts);
    std::cout << "Time for output: " << timerOutput.elapsed() << std::endl;
  }
}

  template<template<typename,unsigned int> typename Vector, typename FlowField, unsigned int dim>
void computeFTLE(const Dune::ParameterTree& config)
{
  const std::string        inputName      = config.get<std::string>         ("general.inputName");
  const std::array<bool,3> periodic       = config.get<std::array<bool,3> > ("general.periodic");
  const unsigned int       inputArrayId   = config.get<unsigned int>        ("general.inputArrayId");
  const std::string        outputName     = config.get<std::string>         ("general.outputName");
  const double             cutoffValue    = config.get<double>              ("general.cutoffValue");

  std::cout << "reading flow field" << std::endl;
  Dune::Timer timerFlowField(false);
  timerFlowField.start();
  FlowField flowField(inputName,periodic,inputArrayId,cutoffValue);
  timerFlowField.stop();
  std::cout << "Time for input: " << timerFlowField.elapsed() << std::endl;

  const Vector<double,dim> center              = config.get<std::array<double,dim> >      ("general.center");
  const Vector<double,dim> extent              = config.get<std::array<double,dim> >      ("general.extent");
  const std::array<unsigned int,dim> intervals = config.get<std::array<unsigned int,dim> >("general.intervals");
  PointGrid<Vector<double,dim>,double,dim> pointGrid(center,extent,intervals);

  const double startTime      = config.get<double>("general.startTime");
  const double timestep       = config.get<double>("general.timestep");
  const double timeResolution = config.get<double>("general.timeResolution");

  const unsigned int dataPartNumber = config.get<unsigned int>("general.dataPartNumber",0);
  const unsigned int dataParts      = config.get<unsigned int>("general.dataParts",1);

  WeightedFlowMap<Vector,FlowField,double,dim> flowMap(flowField,pointGrid,startTime,timeResolution,dataPartNumber,dataParts);

  std::cout << "reading flow map from file" << std::endl;
  flowMap.readFromFile(outputName,startTime + timestep);
  std::cout << "flow map read in" << std::endl;

  ProbDensity<Vector<double,dim>,double,dim> probDensity;
  const std::array<double,dim> radius = config.get<std::array<double,dim> >("general.radius");
  probDensity.setRadius(radius);
  probDensity.setMean(center);
  FTLEComputation<Vector,double,dim> ftleComputation;

  {
    Dune::Timer timerFTLE(false);
    timerFTLE.start();
    std::vector<double> ftleVector;
    const PointSet<Vector<double,dim>,dim>& postSet  = flowMap.post();
    const unsigned int sliceSize = pointGrid.size()/dataParts+((pointGrid.size()%dataParts==0)?0:1);
    std::cout << "pointSet.size " << pointGrid.size() << " " << postSet.size() << " sliceSize " << sliceSize << std::endl;
    for (unsigned int i = dataPartNumber*sliceSize; i < (dataPartNumber+1)*sliceSize && i < pointGrid.size(); i++)
    {
      if (postSet[i].isValid())
      {
        ftleVector.push_back(std::log(ftleComputation.ftle(flowMap,i))/std::abs(timestep));
        if (!std::isfinite(ftleVector.back()))
          ftleVector.back() = 0.;
      }
      else
        ftleVector.push_back(0.);
    }
    timerFTLE.stop();
    std::cout << "Time for FTLE: " << timerFTLE.elapsed() << std::endl;
    VTKWriter vtkWriter;
    vtkWriter.writeScalarData(outputName + "_ftle.vtk",     "ftle",     pointGrid,ftleVector,dataPartNumber,dataParts);
  }

  {
    Dune::Timer timerFTLE(false);
    timerFTLE.start();
    std::vector<double> ftleSpaceVector;
    const PointSet<Vector<double,dim>,dim>& postSet  = flowMap.post();
    const unsigned int sliceSize = pointGrid.size()/dataParts+((pointGrid.size()%dataParts==0)?0:1);
    std::cout << "pointSet.size " << pointGrid.size() << " " << postSet.size() << " sliceSize " << sliceSize << std::endl;
    for (unsigned int i = dataPartNumber*sliceSize; i < (dataPartNumber+1)*sliceSize && i < pointGrid.size(); i++)
    {
      if (postSet[i].isValid())
      {
        const double ftleSpace = ftleComputation.ftleSpace(flowMap,i);
        ftleSpaceVector.push_back(std::log(ftleSpace)/std::abs(timestep));
        if (!std::isfinite(ftleSpaceVector.back()))
          ftleSpaceVector.back() = 0.;
      }
      else
        ftleSpaceVector.push_back(0.);
    }
    timerFTLE.stop();
    std::cout << "Time for FTLE: " << timerFTLE.elapsed() << std::endl;
    VTKWriter vtkWriter;
    vtkWriter.writeScalarData(outputName + "_ftleSpace.vtk","ftleSpace",pointGrid,ftleSpaceVector,dataPartNumber,dataParts);
  }

  {
    Dune::Timer timerFTLE(false);
    timerFTLE.start();
    std::vector<double> ftleParamVector;
    const PointSet<Vector<double,dim>,dim>& postSet  = flowMap.post();
    const unsigned int sliceSize = pointGrid.size()/dataParts+((pointGrid.size()%dataParts==0)?0:1);
    std::cout << "pointSet.size " << pointGrid.size() << " " << postSet.size() << " sliceSize " << sliceSize << std::endl;
    for (unsigned int i = dataPartNumber*sliceSize; i < (dataPartNumber+1)*sliceSize && i < pointGrid.size(); i++)
    {
      if (postSet[i].isValid())
      {
        const double ftleParam = ftleComputation.ftleParam(flowMap,i);
        ftleParamVector.push_back(std::log(ftleParam)/std::abs(timestep));
        if (!std::isfinite(ftleParamVector.back()))
          ftleParamVector.back() = 0.;
      }
      else
        ftleParamVector.push_back(0.);
    }
    timerFTLE.stop();
    std::cout << "Time for FTLE: " << timerFTLE.elapsed() << std::endl;
    VTKWriter vtkWriter;
    vtkWriter.writeScalarData(outputName + "_ftleParam.vtk","ftleParam",pointGrid,ftleParamVector,dataPartNumber,dataParts);
  }
}

  template<template<typename,unsigned int> typename Vector, typename FlowField, unsigned int dim>
void computePCA(const Dune::ParameterTree& config)
{
  const std::string        inputName      = config.get<std::string>         ("general.inputName");
  const std::array<bool,3> periodic       = config.get<std::array<bool,3> > ("general.periodic");
  const unsigned int       inputArrayId   = config.get<unsigned int>        ("general.inputArrayId");
  const std::string        outputName     = config.get<std::string>         ("general.outputName");
  const double             cutoffValue    = config.get<double>              ("general.cutoffValue");

  std::cout << "reading flow field" << std::endl;
  Dune::Timer timerFlowField(false);
  timerFlowField.start();
  FlowField flowField(inputName,periodic,inputArrayId,cutoffValue);
  timerFlowField.stop();
  std::cout << "Time for input: " << timerFlowField.elapsed() << std::endl;

  const Vector<double,dim> center              = config.get<std::array<double,dim> >      ("general.center");
  const Vector<double,dim> extent              = config.get<std::array<double,dim> >      ("general.extent");
  const std::array<unsigned int,dim> intervals = config.get<std::array<unsigned int,dim> >("general.intervals");
  PointGrid<Vector<double,dim>,double,dim> pointGrid(center,extent,intervals);

  const double startTime      = config.get<double>("general.startTime");
  const double timestep       = config.get<double>("general.timestep");
  const double timeResolution = config.get<double>("general.timeResolution");

  const unsigned int dataPartNumber = config.get<unsigned int>("general.dataPartNumber",0);
  const unsigned int dataParts      = config.get<unsigned int>("general.dataParts",1);

  WeightedFlowMap<Vector,FlowField,double,dim> flowMap(flowField,pointGrid,startTime,timeResolution,dataPartNumber,dataParts);


  std::cout << "reading flow map from file" << std::endl;
  flowMap.readFromFile(outputName,startTime + timestep);

  ProbDensity<Vector<double,dim>,double,dim> probDensity;
  const std::array<double,dim> radius = config.get<std::array<double,dim> >("general.radius");
  probDensity.setRadius(radius);
  probDensity.setMean(center);
  Statistics<Vector,double,dim> statistics;
  FTLEComputation<Vector,double,dim> ftleComputation;

  const unsigned int indexWidth = config.get<unsigned int>("general.indexWidth");

  Dune::Timer timerPCA(false);
  timerPCA.start();
  std::vector<double> spectralNormVector;
  std::vector<double> spectralNorm2dVector;
  double radiusSquared = 0.;
  for (unsigned int i = 0; i < dim; i++)
    radiusSquared += radius[i]*radius[i];
  radiusSquared /= dim;
  for (unsigned int i = 0; i < pointGrid.size(); i++)
  {
    flowMap.setWeights(probDensity,i,indexWidth);
    const Vector<double,dim> mean = statistics.mean(flowMap);
    const Matrix<Vector,double,dim> covMat = statistics.covMat(flowMap,mean);
    spectralNormVector.push_back(std::log(std::sqrt(covMat.spectralNorm()/radiusSquared))/std::abs(timestep));
    spectralNorm2dVector.push_back(std::log(std::sqrt(covMat.template partialSpectralNorm<2>()/radiusSquared))/std::abs(timestep));
  }
  timerPCA.stop();
  std::cout << "Time for PCA: " << timerPCA.elapsed() << std::endl;
  Dune::Timer timerPCAOut(false);
  timerPCAOut.start();
  VTKWriter vtkWriter;
  vtkWriter.writeScalarData(outputName + "_spectralNorm.vtk",  "spectralNorm",   pointGrid,spectralNormVector);
  vtkWriter.writeScalarData(outputName + "_2d_spectralNorm.vtk",  "spectralNorm",pointGrid,spectralNorm2dVector);
  timerPCAOut.stop();
  std::cout << "Time for PCA output: " << timerPCAOut.elapsed() << std::endl;

  std::vector<double> spectralNormParamVector;
  std::vector<double> spectralNorm2dParamVector;
  std::array<double,dim> radiusParam = radius;
  for (unsigned int i = 0; i < dim-1; i++)
    radiusParam[i] = 1e-10;
  radiusSquared = radiusParam[dim-1]*radiusParam[dim-1];
  probDensity.setRadius(radiusParam);
  Dune::Timer timerParamPCA(false);
  timerParamPCA.start();
  for (unsigned int i = 0; i < pointGrid.size(); i++)
  {
    flowMap.setWeights(probDensity,i,indexWidth);
    const Vector<double,dim> mean = statistics.mean(flowMap);
    const Matrix<Vector,double,dim> covMat = statistics.covMat(flowMap,mean);
    spectralNormParamVector.push_back(std::log(std::sqrt(covMat.spectralNorm()/radiusSquared))/std::abs(timestep));
    spectralNorm2dParamVector.push_back(std::log(std::sqrt(covMat.template partialSpectralNorm<2>()/radiusSquared))/std::abs(timestep));
  }
  timerParamPCA.stop();
  std::cout << "Time for param PCA: " << timerParamPCA.elapsed() << std::endl;
  std::vector<double> spectralNormSpaceVector;
  std::vector<double> spectralNorm2dSpaceVector;
  std::array<double,dim> radiusSpace = radius;
  radiusSpace[dim-1] = 1e-10;
  radiusSquared = 0.;
  for (unsigned int i = 0; i < dim-1; i++)
    radiusSquared += radiusSpace[i]*radiusSpace[i];
  radiusSquared /= dim-1;
  probDensity.setRadius(radiusSpace);
  Dune::Timer timerSpacePCA(false);
  timerSpacePCA.start();
  for (unsigned int i = 0; i < pointGrid.size(); i++)
  {
    flowMap.setWeights(probDensity,i,indexWidth);
    const Vector<double,dim> mean = statistics.mean(flowMap);
    const Matrix<Vector,double,dim> covMat = statistics.covMat(flowMap,mean);
    spectralNormSpaceVector.push_back(std::log(std::sqrt(covMat.spectralNorm()/radiusSquared))/std::abs(timestep));
    spectralNorm2dSpaceVector.push_back(std::log(std::sqrt(covMat.template partialSpectralNorm<2>()/radiusSquared))/std::abs(timestep));
  }
  timerSpacePCA.stop();
  std::cout << "Time for space PCA: " << timerSpacePCA.elapsed() << std::endl;
  Dune::Timer timerPCAOut2(false);
  timerPCAOut2.start();
  vtkWriter.writeScalarData(outputName + "_param_spectralNorm.vtk",   "spectralNorm",pointGrid,spectralNormParamVector);
  vtkWriter.writeScalarData(outputName + "_space_spectralNorm.vtk",   "spectralNorm",pointGrid,spectralNormSpaceVector);
  vtkWriter.writeScalarData(outputName + "_2d_param_spectralNorm.vtk","spectralNorm",pointGrid,spectralNorm2dParamVector);
  vtkWriter.writeScalarData(outputName + "_2d_space_spectralNorm.vtk","spectralNorm",pointGrid,spectralNorm2dSpaceVector);
  timerPCAOut2.stop();
  std::cout << "Time for secondary PCA output: " << timerPCAOut2.elapsed() << std::endl;

  Dune::Timer timerFTLE(false);
  timerFTLE.start();
  std::vector<double> ftleVector;
  for (unsigned int i = 0; i < pointGrid.size(); i++)
  {
    ftleVector.push_back(std::log(ftleComputation.ftle(flowMap,i))/std::abs(timestep));
  }
  timerFTLE.stop();
  std::cout << "Time for FTLE: " << timerFTLE.elapsed() << std::endl;

  std::vector<std::vector<double> > spectralNormsVector(10);
  std::vector<std::vector<double> > normsDiffVector(10);
  std::array<double,dim> scaledRadius = radius;
  for (int i = 0; i < 10; i++)
  {
    std::cout << "radius:";
    for (unsigned int j = 0; j < dim; j++)
    {
      scaledRadius[j] = radius[j] * std::pow(1.1,9-i);
      std::cout << " " << scaledRadius[j];
    }
    std::cout << std::endl;

    probDensity.setRadius(scaledRadius);
    radiusSquared = 0.;
    for (unsigned int j = 0; j < dim; j++)
      radiusSquared += radius[j]*radius[j];
    radiusSquared /= dim;
    for (unsigned int j = 0; j < pointGrid.size(); j++)
    {
      flowMap.setWeights(probDensity,j,indexWidth);
      const Vector<double,dim> mean = statistics.mean(flowMap);
      const Matrix<Vector,double,dim> covMat = statistics.covMat(flowMap,mean);
      spectralNormsVector[i].push_back(std::log(std::sqrt(covMat.spectralNorm()/radiusSquared))/std::abs(timestep));
      normsDiffVector[i].push_back(spectralNormsVector[i][j] - ftleVector[j]);
    }

    std::stringstream s;
    s << i;
    std::string iter;
    s >> iter;

    vtkWriter.writeScalarData(outputName+"_spectralNorms_"+iter+".vtk","spectralNorms",pointGrid,spectralNormsVector[i]);
    vtkWriter.writeScalarData(outputName+"_normsDiff_"+iter+".vtk","normsDiff",pointGrid,normsDiffVector[i]);
  }

  std::vector<std::vector<double> > normsOrderVector(9);
  std::vector<std::vector<double> > normsDerivVector(9);
  for (int i = 0; i < 8; i++)
  {
    for (unsigned int j = 0; j < pointGrid.size(); j++)
    {
      normsOrderVector[i].push_back(std::log((spectralNormsVector[i+1][j]-spectralNormsVector[8][j])/(spectralNormsVector[i][j]-spectralNormsVector[8][j]))/std::log(1/1.1));
      normsDerivVector[i].push_back((spectralNormsVector[i+1][j]-spectralNormsVector[i][j])/0.1);
    }

    std::stringstream s;
    s << i;
    std::string iter;
    s >> iter;

    vtkWriter.writeScalarData(outputName+"_normsOrder_"+iter+".vtk","normsOrder",pointGrid,normsOrderVector[i]);
    vtkWriter.writeScalarData(outputName+"_normsDeriv_"+iter+".vtk","normsDeriv",pointGrid,normsDerivVector[i]);
  }
}

  template<template<typename,unsigned int> typename Vector, typename FlowField, unsigned int dim>
void computeTrajectories(const Dune::ParameterTree& config)
{
  const std::string        inputName      = config.get<std::string>         ("general.inputName");
  const std::array<bool,3> periodic       = config.get<std::array<bool,3> > ("general.periodic");
  const unsigned int       inputArrayId   = config.get<unsigned int>        ("general.inputArrayId");
  const std::string        outputName     = config.get<std::string>         ("general.outputName");
  const double             cutoffValue    = config.get<double>              ("general.cutoffValue");

  std::cout << "reading flow field" << std::endl;
  Dune::Timer timerFlowField(false);
  timerFlowField.start();
  FlowField flowField(inputName,periodic,inputArrayId,cutoffValue);
  timerFlowField.stop();
  std::cout << "Time for input: " << timerFlowField.elapsed() << std::endl;

  const Vector<double,dim> center              = config.get<std::array<double,dim> >      ("general.center");
  const Vector<double,dim> extent              = config.get<std::array<double,dim> >      ("general.extent");
  const std::array<unsigned int,dim> intervals = config.get<std::array<unsigned int,dim> >("general.intervals");

  const double startTime      = config.get<double>("general.startTime");
  const double timestep       = config.get<double>("general.timestep");
  const double timeResolution = config.get<double>("general.timeResolution");

  const unsigned int sampleFactor = config.get<unsigned int>("general.sampleFactor");
  std::array<unsigned int,dim> coarseIntervals = intervals;
  for (unsigned int i = 0; i < dim; i++)
    coarseIntervals[i] /= sampleFactor;

  Dune::Timer timerTrajectory(false);
  timerTrajectory.start();
  PointGrid<Vector<double,dim>,double,dim> coarseGrid(center,extent,coarseIntervals);
  VTKWriter vtkWriter;
  for (unsigned int i = 0; i < coarseGrid.size(); i++)
  {
    RungeKutta<Vector,double,dim> rungeKutta;
    std::vector<Vector<double,dim> > path;
    const unsigned int substeps = std::abs(timestep)/timeResolution;
    rungeKutta.step(flowField,coarseGrid[i],startTime,timestep,substeps,10,path);
    std::array<unsigned int,dim> indices;
    coarseGrid.indexToIndices(i,indices);
    std::stringstream s;
    for (unsigned int j = 0; j < dim; j++)
      s << "_" << indices[j];
    std::string iter;
    s >> iter;
    vtkWriter.writeLineData(outputName + "_line" + iter + ".vtk","line" + iter,coarseGrid,path);
  }
  timerTrajectory.stop();
  std::cout << "Time for trajectory output: " << timerTrajectory.elapsed() << std::endl;
}

  template<template<typename,unsigned int> typename Vector, typename FlowField, unsigned int dim>
void driver(const Dune::ParameterTree& config)
{
  config.report(std::cout);

  if (config.get<bool>("general.computeFlowMap"))
  {
    computeFlowMap<Vector,FlowField,dim>(config);
    return;
  }
  else if (config.get<bool>("general.computeFTLE"))
  {
    computeFTLE<Vector,FlowField,dim>(config);
    return;
  }
  else if (config.get<bool>("general.computePCA"))
  {
    computePCA<Vector,FlowField,dim>(config);
    return;
  }
  else if (config.get<bool>("general.computeTrajectories"))
  {
    computeTrajectories<Vector,FlowField,dim>(config);
    return;
  }
  else
  {
    std::cout << "Nothing to do, computeFlowMap, computeFTLE or computePCA" << std::endl;
  }

}

void dispatcher(const Dune::ParameterTree& config)
{
  const unsigned int dim = config.get<unsigned int>("general.dim");
  const std::string datasetType = config.get<std::string>("general.datasetType");
  const std::string vectorType = config.get<std::string>("general.vectorType");

  if (dim == 2)
  {
    if (datasetType == "vtk")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,VTKReader,2>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,VTKReader,2>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else if (datasetType == "pvd")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,PVDReader,2>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,PVDReader,2>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else if (datasetType == "synth")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,SyntheticVectorField,2>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,SyntheticVectorField,2>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else
      DUNE_THROW(Dune::Exception,"dataset type must be vtk, pvd or synth in 2D");
  }
  else if (dim == 3)
  {
    if (datasetType == "vtk")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,VTKReader,3>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,VTKReader,3>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else if (datasetType == "pvd")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,PVDReader,3>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,PVDReader,3>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else if (datasetType == "ensemble")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,ParamEnsemblePVDReader,3>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,ParamEnsemblePVDReader,3>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else if (datasetType == "ensembleFromTime")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,EnsembleFromTimeReader,3>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,EnsembleFromTimeReader,3>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else if (datasetType == "synth")
    {
      if (vectorType == "cartesian")
      {
        driver<CartesianVector,SyntheticVectorField,3>(config);
      }
      else if (vectorType == "spherical")
      {
        driver<SphericalVector,SyntheticVectorField,3>(config);
      }
      else
        DUNE_THROW(Dune::Exception,"vector type must be cartesian or spherical");
    }
    else
      DUNE_THROW(Dune::Exception,"dataset type must be vtk, pvd, ensemble, ensembleFromTime or synth in 3D");
  }
  else
    DUNE_THROW(Dune::Exception,"dimension must be 2 or 3");
}

#endif // DUNE_VISU_HH
