#ifndef DUNE_VISU_RUNGEKUTTA_HH
#define DUNE_VISU_RUNGEKUTTA_HH

/**
 * implementation of classical Runge-Kutta RK4 method
 */
template<template<typename,unsigned int> typename Vector, typename Range, unsigned int dim>
class RungeKutta
{
  private:

    Vector<Range,dim> k1,k2,k3,k4,y1,y2,y3;

  public:

    RungeKutta()
    {}

    template<typename VectorField>
      void step(const VectorField& vectorField, const Vector<Range,dim>& start, const Range& startTime, const Range& timestep, Vector<Range,dim>& end)
      {
        bool keepValid = true;
        const Range halfTime = startTime + 0.5 * timestep;
        const Range endTime  = startTime + timestep;

        keepValid = keepValid && vectorField.template eval<Vector<Range,dim>,Range,dim>(start,startTime,k1);
        y1 = start + 0.5 * timestep * k1;
        keepValid = keepValid && vectorField.template eval<Vector<Range,dim>,Range,dim>(y1,halfTime,k2);
        y2 = start + 0.5 * timestep * k2;
        keepValid = keepValid && vectorField.template eval<Vector<Range,dim>,Range,dim>(y2,halfTime,k3);
        y3 = start + timestep * k3;
        keepValid = keepValid && vectorField.template eval<Vector<Range,dim>,Range,dim>(y3,endTime,k4);

        end = start + (1./6.) * timestep * (k1 + 2.*k2 + 2.*k3 + k4);
      }

    template<typename VectorField>
      void step(const VectorField& vectorField, const Vector<Range,dim>& start, const Range& startTime, const Range& timestep, const unsigned int substeps, Vector<Range,dim>& end)
      {
        bool wasInvalidBefore = false;

        const Range smallStep = timestep / substeps;
        Range currentTime = startTime;

        Vector<Range,dim> intermediate = start;
        for (unsigned int i = 0; i < substeps - 1; i++)
        {
          step(vectorField,intermediate,currentTime,smallStep,end);

          if (!end.isValid())
          {
            if (wasInvalidBefore)
              return;
            else
            {
              end.makeValid();
              wasInvalidBefore = true;
            }
          }
          else
            wasInvalidBefore = false;

          intermediate = end;
          currentTime += smallStep;
        }
        step(vectorField,intermediate,currentTime,smallStep,end);
      }

    template<typename VectorField>
      void step(const VectorField& vectorField, const Vector<Range,dim>& start, const Range& startTime, const Range& timestep, const unsigned int substeps, const unsigned int writeEvery, std::vector<Vector<Range,dim> >& path)
      {
        path.clear();
        path.push_back(start);

        if (!start.isValid())
          return;

        const Range smallStep = timestep / substeps;
        Range currentTime = startTime;

        Vector<Range,dim> intermediate = start;
        Vector<Range,dim> result;
        for (unsigned int i = 0; i < substeps - 1; i++)
        {
          step(vectorField,intermediate,currentTime,smallStep,result);
          if (!result.isValid())
            return;
          if (i % writeEvery == 0)
            path.push_back(result);
          intermediate = result;
          currentTime += smallStep;
        }
        step(vectorField,intermediate,currentTime,smallStep,result);
        if (!result.isValid())
          return;
        path.push_back(result);
      }

    template<typename VectorField>
      void step(const VectorField& vectorField, const PointGrid<Vector<Range,dim>,Range,dim> start, const Range& startTime, const Range& timestep, const unsigned int substeps, PointSet<Vector<Range,dim>,dim>& end, const unsigned int dataPartNumber = 0, const unsigned int dataParts = 1)
      {
        Dune::Timer rungeKuttaTimer(false);
        rungeKuttaTimer.start();
        unsigned int sliceSize = start.size()/dataParts;
        if (start.size() % dataParts != 0)
          sliceSize++;
        std::cout << "numPoints: " << start.size() << " sliceSize: " << sliceSize << " dataPartNumber: " << dataPartNumber << " dataParts: " << dataParts << std::endl;
        for (unsigned int i = dataPartNumber * sliceSize; i < (dataPartNumber+1) * sliceSize && i < start.size(); i++)
        {
          if ((i + 1 - dataPartNumber*sliceSize) % (sliceSize / 100) == 0)
          {
            unsigned int elapsed = rungeKuttaTimer.elapsed();
            unsigned int percent = ((i + 1 - dataPartNumber*sliceSize)/(sliceSize/100));
            std::cout << percent << "%, elapsed: " << elapsed/60. << " min, eta: " << (100. - percent)/percent * (elapsed/60.) << " min" << std::endl;
          }
          step(vectorField,start[i],startTime,timestep,substeps,end[i]);
        }

        unsigned int invalid = 0;
        for (unsigned int i = dataPartNumber * sliceSize; i < (dataPartNumber+1) * sliceSize && i < start.size(); i++)
          if (!(end[i].isValid()))
            invalid++;
        std::cout << "RungeKutta: invalid points " << invalid << " / " << start.size() << " (" << (double)invalid/(double)start.size()*100. << "%)" << std::endl;
      }

};

/**
 * implementation of Runge-Kutta-Fehlberg method
 */
template<template<typename,unsigned int> typename Vector, typename Range, unsigned int dim>
class RungeKuttaFehlberg
{
  private:

    std::array<Vector<Range,dim>,5> k;
    std::array<Vector<Range,dim>,5> y;

    Vector<Range,dim> error;
  public:

    RungeKuttaFehlberg()
    {}

    template<typename VectorField>
      void step(const VectorField& vectorField, const Vector<Range,dim>& start, const Range& startTime, const Range& timestep, Vector<Range,dim>& end)
      {
        if (!start.isValid())
          return;

        vectorField.template eval<Vector<Range,dim>,Range,dim>(start,startTime,k[0]);
        y[0] = start + 1./4. * timestep * k[0];
        vectorField.template eval<Vector<Range,dim>,Range,dim>(y[0],startTime + 1./4. * timestep,k[1]);
        y[1] = start + timestep * (3./32. * k[0] + 9./32. * k[1]);
        vectorField.template eval<Vector<Range,dim>,Range,dim>(y[1],startTime + 3./8. * timestep,k[2]);
        y[2] = start + timestep * (1932./2197. * k[0] - 7200./2197. * k[1] + 7296./2197. * k[2]);
        vectorField.template eval<Vector<Range,dim>,Range,dim>(y[2],startTime + 12./13. * timestep,k[3]);
        y[3] = start + timestep * (439./216. * k[0] - 8. * k[1] + 3680./513. * k[2] - 845./4104. * k[3]);
        vectorField.template eval<Vector<Range,dim>,Range,dim>(y[3],startTime + timestep,k[4]);
        y[4] = start + timestep * (-8./27. * k[0] + 2. * k[1] - 3544./2565. * k[2] + 1859./4104. * k[3] - 11./40. * k[4]);

        end = start + timestep * (16./135. * k[0] + 6656./12825. * k[2] + 28561./56430. * k[3] - 9./50. * k[4] + 2./55. * k[5]);
        error = start + timestep * (25./216. * k[0] + 1408./2565. * k[2] + 2197./4104. * k[3] - 1./5. * k[4]);
        error -= end;
      }

    template<typename VectorField>
      void step(const VectorField& vectorField, const Vector<Range,dim>& start, const Range& startTime, const Range& timestep, const unsigned int substeps, Vector<Range,dim>& end)
      {
        if (!start.isValid())
          return;

        const Range smallStep = timestep / substeps;
        Range currentTime = startTime;

        Vector<Range,dim> intermediate = start;
        for (unsigned int i = 0; i < substeps - 1; i++)
        {
          step(vectorField,intermediate,currentTime,smallStep,end);
          intermediate = end;
          currentTime += smallStep;
        }
        step(vectorField,intermediate,currentTime,smallStep,end);
      }

    template<typename VectorField>
      void step(const VectorField& vectorField, const PointGrid<Vector<Range,dim>,Range,dim> start, const Range& startTime, const Range& timestep, const unsigned int substeps, PointSet<Vector<Range,dim>,dim>& end)
      {
        for (unsigned int i = 0; i < start.size(); i++)
          step(vectorField,start[i],startTime,timestep,substeps,end[i]);
      }

};

#endif // DUNE_VISU_RUNGEKUTTA_HH
