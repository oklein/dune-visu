#ifndef DUNE_VISU_FTLE_HH
#define DUNE_VISU_FTLE_HH

#include<dune/visu/vector.hh>
#include<dune/visu/matrix.hh>

template<template<typename,unsigned int> typename Vector, typename Range, unsigned int dim>
class FTLEComputation
{
  public:

    template<typename FlowMap>
      Matrix<Vector,Range,dim> flowMapGradient(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim> output;
        const PointGrid<Vector<Range,dim>,Range,dim>& preSet  = flowMap.pre();
        const PointSet<Vector<Range,dim>,dim>&        postSet = flowMap.post();

        std::array<unsigned int,dim> intervals = preSet.gridIntervals();
        std::array<unsigned int,dim> indices;
        flowMap.pre().indexToIndices(index,indices);
        std::array<unsigned int,dim> shiftIndices = indices;
        unsigned int shiftIndex;

        Vector<Range,dim> preDiff, postDiff;
        for (unsigned int i = 0; i < dim; i++)
        {
          shiftIndices[i]++;
          if (shiftIndices[0] < intervals[0]+1 && shiftIndices[1] < intervals[1]+1
              && (dim == 2 || shiftIndices[2] < intervals[2]+1))
          {
            flowMap.pre().indicesToIndex(shiftIndices,shiftIndex);
            preDiff  = preSet[shiftIndex];
            postDiff = postSet[shiftIndex];
          }
          else
          {
            preDiff  = preSet[index];
            postDiff = postSet[index];
          }
          shiftIndices[i]--;

          shiftIndices[i]--;
          if (shiftIndices[0] < intervals[0]+1 && shiftIndices[1] < intervals[1]+1
              && (dim == 2 || shiftIndices[2] < intervals[2]+1))
          {
            flowMap.pre().indicesToIndex(shiftIndices,shiftIndex);
            preDiff  -= preSet[shiftIndex];
            postDiff -= postSet[shiftIndex];
          }
          else
          {
            preDiff  -= preSet[index];
            postDiff -= postSet[index];
          }
          shiftIndices[i]++;

          for (unsigned int j = 0; j < dim; j++)
            output(j,i) = postDiff[j] / preDiff[i];
        }

        return output;
      }

    template<typename FlowMap>
      Matrix<Vector,Range,dim-1> flowMapSpatialGradient(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim-1> output;
        const PointGrid<Vector<Range,dim>,Range,dim>& preSet  = flowMap.pre();
        const PointSet<Vector<Range,dim>,dim>&        postSet = flowMap.post();

        std::array<unsigned int,dim> intervals = preSet.gridIntervals();
        std::array<unsigned int,dim> indices;
        flowMap.pre().indexToIndices(index,indices);
        std::array<unsigned int,dim> shiftIndices = indices;
        unsigned int shiftIndex;

        Vector<Range,dim> preDiff, postDiff;
        for (unsigned int i = 0; i < dim-1; i++)
        {
          shiftIndices[i]++;
          if (shiftIndices[0] < intervals[0]+1 && shiftIndices[1] < intervals[1]+1
              && (dim == 2 || shiftIndices[2] < intervals[2]+1))
          {
            flowMap.pre().indicesToIndex(shiftIndices,shiftIndex);
            preDiff  = preSet[shiftIndex];
            postDiff = postSet[shiftIndex];
          }
          else
          {
            preDiff  = preSet[index];
            postDiff = postSet[index];
          }
          shiftIndices[i]--;

          shiftIndices[i]--;
          if (shiftIndices[0] < intervals[0]+1 && shiftIndices[1] < intervals[1]+1
              && (dim == 2 || shiftIndices[2] < intervals[2]+1))
          {
            flowMap.pre().indicesToIndex(shiftIndices,shiftIndex);
            preDiff  -= preSet[shiftIndex];
            postDiff -= postSet[shiftIndex];
          }
          else
          {
            preDiff  -= preSet[index];
            postDiff -= postSet[index];
          }
          shiftIndices[i]++;

          for (unsigned int j = 0; j < dim-1; j++)
            output(j,i) = postDiff[j] / preDiff[i];
        }

        return output;
      }

    template<typename FlowMap>
      Vector<Range,dim-1> flowMapParamGradient(const FlowMap& flowMap, unsigned int index) const
      {
        Vector<Range,dim-1> output;
        const PointGrid<Vector<Range,dim>,Range,dim>& preSet  = flowMap.pre();
        const PointSet<Vector<Range,dim>,dim>&        postSet = flowMap.post();

        std::array<unsigned int,dim> intervals = preSet.gridIntervals();
        std::array<unsigned int,dim> indices;
        flowMap.pre().indexToIndices(index,indices);
        std::array<unsigned int,dim> shiftIndices = indices;
        unsigned int shiftIndex;

        Vector<Range,dim> preDiff, postDiff;
        shiftIndices[dim-1]++;
        if (shiftIndices[0] < intervals[0]+1 && shiftIndices[1] < intervals[1]+1
            && (dim == 2 || shiftIndices[2] < intervals[2]+1))
        {
          flowMap.pre().indicesToIndex(shiftIndices,shiftIndex);
          preDiff  = preSet[shiftIndex];
          postDiff = postSet[shiftIndex];
        }
        else
        {
          preDiff  = preSet[index];
          postDiff = postSet[index];
        }
        shiftIndices[dim-1]--;

        shiftIndices[dim-1]--;
        if (shiftIndices[0] < intervals[0]+1 && shiftIndices[1] < intervals[1]+1
            && (dim == 2 || shiftIndices[2] < intervals[2]+1))
        {
          flowMap.pre().indicesToIndex(shiftIndices,shiftIndex);
          preDiff  -= preSet[shiftIndex];
          postDiff -= postSet[shiftIndex];
        }
        else
        {
          preDiff  -= preSet[index];
          postDiff -= postSet[index];
        }
        shiftIndices[dim-1]++;

        for (unsigned int j = 0; j < dim-1; j++)
          output[j] = postDiff[j] / preDiff[dim-1];

        return output;
      }

    template<typename FlowMap>
      Range ftle(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim> gradient = flowMapGradient(flowMap,index);

        Vector<Range,dim> singular = gradient.singular();

        Range output = 0.;
        for (unsigned int i = 0; i < dim; i++)
          if (output < singular[i])
            output = singular[i];

        return output;
      }

    template<typename FlowMap>
      Range ftleSpace(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim-1> gradient = flowMapSpatialGradient(flowMap,index);

        Vector<Range,dim-1> singular = gradient.singular();

        Range output = 0.;
        for (unsigned int i = 0; i < dim-1; i++)
          if (output < singular[i])
            output = singular[i];

        return output;
      }

    template<typename FlowMap>
      Range ftleParam(const FlowMap& flowMap, unsigned int index) const
      {
        Vector<Range,dim-1> gradient = flowMapParamGradient(flowMap,index);

        Range output = 0.;
        for (unsigned int i = 0; i < dim-1; i++)
          if (output < std::abs(gradient[i]))
            output = std::abs(gradient[i]);

        return output;
      }


    template<typename FlowMap>
      Range eccentricity(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim> gradient = flowMapGradient(flowMap,index);

        Vector<Range,dim> singular = gradient.singular();

        Range singMax = singular[0], singMin = singular[1];
        for (unsigned int i = 0; i < dim; i++)
        {
          if (singMax < singular[i])
            singMax = singular[i];
          if (singMin > singular[i] && singular[i] > 1e-6)
            singMin = singular[i];
        }

        Range output = std::sqrt(1. - singMin/singMax);

        return output;
      }

    template<typename FlowMap>
      Range eccentricity2(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim> gradient = flowMapGradient(flowMap,index);

        Vector<Range,dim> singular = gradient.singular();

        Range singMax = singular[0], singMin = singular[1];
        for (unsigned int i = 0; i < dim; i++)
        {
          if (singMax < singular[i])
            singMax = singular[i];
          if (singMin > singular[i] && singular[i] > 1e-6)
            singMin = singular[i];
        }

        Range output = std::sqrt(singMax/singMin - 1.);

        return output;
      }

    template<typename FlowMap>
      Range eccentricity3(const FlowMap& flowMap, unsigned int index) const
      {
        Matrix<Vector,Range,dim> gradient = flowMapGradient(flowMap,index);

        Vector<Range,dim> singular = gradient.singular();

        Range singMax = singular[0], singMin = singular[1];
        for (unsigned int i = 0; i < dim; i++)
        {
          if (singMax < singular[i])
            singMax = singular[i];
          if (singMin > singular[i] && singular[i] > 1e-6)
            singMin = singular[i];
        }

        Range output = std::sqrt(singMax-singMin)/std::sqrt(singMax+singMin);

        return output;
      }

};

#endif // DUNE_VISU_FTLE_HH
