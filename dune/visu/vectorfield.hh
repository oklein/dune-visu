#ifndef DUNE_VISU_VECTORFIELD_HH
#define DUNE_VISU_VECTORFIELD_HH

class SyntheticVectorField
{
  enum FieldType {Circle, Lorenz, DoubleGyreEpsilon, DoubleGyreOmega};

  FieldType type;

  public:

  SyntheticVectorField(const std::string& fileName, const std::array<bool,3>& periodic, const unsigned int arrayId, const double cutOffValue)
  {
    if (fileName == "circle")
      type = Circle;
    else if (fileName == "lorenz")
      type = Lorenz;
    else if (fileName == "doubleGyreEpsilon")
      type = DoubleGyreEpsilon;
    else if (fileName == "doubleGyreOmega")
      type = DoubleGyreOmega;
    else
      DUNE_THROW(Dune::Exception, "type for synthetic flow field not understood");
  }

  template<typename Vector, typename Range, unsigned int dim>
    bool eval(const Vector& point, const Range& time, Vector& vector) const
    {
      if (type == Circle)
        return evalCircle<Vector,Range,dim>(point,time,vector);
      else if (type == Lorenz)
        return evalLorenz<Vector,Range,dim>(point,time,vector);
      else if (type == DoubleGyreEpsilon)
        return evalDoubleGyreEpsilon<Vector,Range,dim>(point,time,vector);
      else if (type == DoubleGyreOmega)
        return evalDoubleGyreOmega<Vector,Range,dim>(point,time,vector);

      return false;
    }

  private:

  template<typename Vector, typename Range, unsigned int dim>
    bool evalCircle(const Vector& point, const Range& time, Vector& vector) const
    {
      if (dim != 2)
        DUNE_THROW(Dune::Exception, "flow field only defined in two dimensions");

      vector[0] =  point[1];
      vector[1] = -point[0];

      return true;
    }

  template<typename Vector, typename Range, unsigned int dim>
    bool evalLorenz(const Vector& point, const Range& time, Vector& vector) const
    {
      if (dim != 3)
        DUNE_THROW(Dune::Exception, "flow field only defined in three dimensions");

      vector[0] = 10. * (point[1] - point[0]);
      vector[1] = point[0] * (28. - point[2]) - point[1];
      vector[2] = point[0] * point[1] - 8./3. * point[2];

      return true;
    }

  template<typename Vector, typename Range, unsigned int dim>
    bool evalDoubleGyreEpsilon(const Vector& point, const Range& time, Vector& vector) const
    {
      if (dim != 3)
        DUNE_THROW(Dune::Exception, "flow field only defined in three dimensions");

      const Range x   = point[0];
      const Range y   = point[1];
      const Range eps = point[2];

      const Range A     = 0.1;
      const Range omega = 0.2*M_PI;

      const Range sinomt = std::sin(omega*time);
      const Range a      = eps * sinomt;
      const Range b      = 1. - 2.*eps*sinomt;
      const Range f      = a * x*x + b * x;
      const Range dxf    = 2.*a * x + b;

      vector[0] = -M_PI * A * std::sin(M_PI*f) * std::cos(M_PI*y);
      vector[1] =  M_PI * A * std::cos(M_PI*f) * std::sin(M_PI*y) * dxf;
      vector[2] = 0.;

      return true;
    }

  template<typename Vector, typename Range, unsigned int dim>
    bool evalDoubleGyreOmega(const Vector& point, const Range& time, Vector& vector) const
    {
      if (dim != 3)
        DUNE_THROW(Dune::Exception, "flow field only defined in three dimensions");

      const Range x = point[0];
      const Range y = point[1];
      const Range A = point[2];

      const Range eps   = 0.1;
      const Range omega = 0.2*M_PI;

      const Range sinomt = std::sin(omega*time);
      const Range a      = eps * sinomt;
      const Range b      = 1. - 2.*eps*sinomt;
      const Range f      = a * x*x + b * x;
      const Range dxf    = 2.*a * x + b;

      vector[0] = -M_PI * A * std::sin(M_PI*f) * std::cos(M_PI*y);
      vector[1] =  M_PI * A * std::cos(M_PI*f) * std::sin(M_PI*y) * dxf;
      vector[2] = 0.;

      return true;
    }

};

#endif // DUNE_VISU_VECTORFIELD_HH
