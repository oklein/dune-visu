#ifndef DUNE_VISU_STATISTICS_HH
#define DUNE_VISU_STATISTICS_HH

#include<dune/visu/vector.hh>
#include<dune/visu/matrix.hh>
#include<dune/visu/tensor3.hh>
#include<dune/visu/tensor4.hh>

template<template<typename,unsigned int> typename Vector, typename Range, unsigned int dim>
class Statistics
{
  public:

    template<typename FlowMap>
      Vector<Range,dim> mean(const FlowMap& flowMap) const
      {
        Vector<Range,dim> output;
        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          output += weightedIndices[i].second * postSet[weightedIndices[i].first];

        return output;
      }

    template<typename FlowMap>
      Matrix<Vector,Range,dim> covMat(const FlowMap& flowMap, const Vector<Range,dim>& mean) const
      {
        Matrix<Vector,Range,dim> output;
        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          for (unsigned int j = 0; j < dim; j++)
            for (unsigned int k = j; k < dim; k++)
              output(j,k) += weightedIndices[i].second * (postSet[weightedIndices[i].first][j] - mean[j]) * (postSet[weightedIndices[i].first][k] - mean[k]);

        for (unsigned int i = 0; i < dim; i++)
          for (unsigned int j = i + 1; j < dim; j++)
            output(j,i) = output(i,j);

        return output;
      }

    // Mardia (1970), Klar (?)
    template<typename FlowMap>
      Range skewMeasure(const FlowMap& flowMap, const Vector<Range,dim>& mean, const Matrix<Vector,Range,dim>& covMat) const
      {
        Range output = 0.;

        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        std::vector<Vector<Range,dim> > standardizedSamples;
        Vector<Range,dim> vector;

        for (unsigned int i = 0; i < weightedIndices.size(); i++)
        {
          vector = postSet[weightedIndices[i].first];
          vector -= mean;
          covMat.timesRootInverse(vector);
          standardizedSamples.push_back(vector);
        }

        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          for (unsigned int j = 0; j < weightedIndices.size(); j++)
          {
            output += weightedIndices[i].second * weightedIndices[j].second * std::pow(standardizedSamples[i] * standardizedSamples[j],3);
          }

        return output;
      }

    // Mori et al (1996), Klar (?)
    template<typename FlowMap>
      Range skewMeasure2(const FlowMap& flowMap, const Vector<Range,dim>& mean, const Matrix<Vector,Range,dim>& covMat) const
      {
        Range output = 0.;

        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        std::vector<Vector<Range,dim> > standardizedSamples;
        Vector<Range,dim> vector;

        for (unsigned int i = 0; i < weightedIndices.size(); i++)
        {
          vector = postSet[weightedIndices[i].first];
          vector -= mean;
          covMat.timesRootInverse(vector);
          standardizedSamples.push_back(vector);
        }

        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          for (unsigned int j = 0; j < weightedIndices.size(); j++)
          {
            output += weightedIndices[i].second * weightedIndices[j].second * (standardizedSamples[i]*standardizedSamples[j]) * (standardizedSamples[i]*standardizedSamples[i]) * (standardizedSamples[j]*standardizedSamples[j]);
          }

        return output;
      }

    // Koziol (1989), Klar (?)
    template<typename FlowMap>
      Range kurtMeasure(const FlowMap& flowMap, const Vector<Range,dim>& mean, const Matrix<Vector,Range,dim>& covMat) const
      {
        Range output = 0.;

        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        std::vector<Vector<Range,dim> > standardizedSamples;
        Vector<Range,dim> vector;

        for (unsigned int i = 0; i < weightedIndices.size(); i++)
        {
          vector = postSet[weightedIndices[i].first];
          vector -= mean;
          covMat.timesRootInverse(vector);
          standardizedSamples.push_back(vector);
        }

        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          for (unsigned int j = 0; j < weightedIndices.size(); j++)
          {
            output += weightedIndices[i].second * weightedIndices[j].second * std::pow(standardizedSamples[i] * standardizedSamples[j],4);
          }

        return output;
      }

    template<typename FlowMap>
      Tensor3<Range,dim> skewTensor(const FlowMap& flowMap, const Vector<Range,dim>& mean) const
      {
        Tensor3<Range,dim> output;
        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          for (unsigned int j = 0; j < dim; j++)
            for (unsigned int k = j; k < dim; k++)
              for (unsigned int l = k; l < dim; l++)
                output(j,k,l) += weightedIndices[i].second * (postSet[weightedIndices[i].first][j] - mean[j]) * (postSet[weightedIndices[i].first][k] - mean[k]) * (postSet[weightedIndices[i].first][l] - mean[l]);

        for (unsigned int j = 0; j < dim; j++)
          for (unsigned int k = j + 1; k < dim; k++)
            for (unsigned int l = k + 1; l < dim; l++)
            {
              output(k,l,j) = output(j,k,l);
              output(l,j,k) = output(j,k,l);
              output(l,k,j) = output(j,k,l);
              output(k,j,l) = output(j,k,l);
              output(j,l,k) = output(j,k,l);
            }

        return output;
      }

    template<typename FlowMap>
      Tensor4<Range,dim> kurtTensor(const FlowMap& flowMap, const Vector<Range,dim>& mean, const Matrix<Vector,Range,dim>& covMat) const
      {
        Tensor4<Range,dim> output;
        const PointSet<Vector<Range,dim>,dim>& postSet = flowMap.post();
        const std::vector<std::pair<unsigned int, Range> >& weightedIndices = flowMap.weightedIndices();
        for (unsigned int i = 0; i < weightedIndices.size(); i++)
          for (unsigned int j = 0; j < dim; j++)
            for (unsigned int k = j; k < dim; k++)
              for (unsigned int l = k; l < dim; l++)
                for (unsigned int m = l; m < dim; m++)
                {
                  output(j,k,l,m) += weightedIndices[i].second * (postSet[weightedIndices[i].first][j] - mean[j]) * (postSet[weightedIndices[i].first][k] - mean[k]) * (postSet[weightedIndices[i].first][l] - mean[l]) * (postSet[weightedIndices[i].first][m] - mean[m]);
                  output(j,k,l,m) -= covMat(j,k)*covMat(l,m) + covMat(j,l)*covMat(k,m) + covMat(j,m)*covMat(k,l);
                }

        for (unsigned int j = 0; j < dim; j++)
          for (unsigned int k = j + 1; k < dim; k++)
            for (unsigned int l = k + 1; l < dim; l++)
              for (unsigned int m = l + 1; m < dim; m++)
              {
                output(k,l,j,m) = output(j,k,l,m);
                output(l,j,k,m) = output(j,k,l,m);
                output(l,k,j,m) = output(j,k,l,m);
                output(k,j,l,m) = output(j,k,l,m);
                output(j,l,k,m) = output(j,k,l,m);
                output(m,j,k,l) = output(j,k,l,m);
                output(m,k,l,j) = output(j,k,l,m);
                output(m,l,j,k) = output(j,k,l,m);
                output(m,l,k,j) = output(j,k,l,m);
                output(m,k,j,l) = output(j,k,l,m);
                output(m,j,l,k) = output(j,k,l,m);
                output(j,m,k,l) = output(j,k,l,m);
                output(k,m,l,j) = output(j,k,l,m);
                output(l,m,j,k) = output(j,k,l,m);
                output(l,m,k,j) = output(j,k,l,m);
                output(k,m,j,l) = output(j,k,l,m);
                output(j,m,l,k) = output(j,k,l,m);
                output(j,k,m,l) = output(j,k,l,m);
                output(k,l,m,j) = output(j,k,l,m);
                output(l,j,m,k) = output(j,k,l,m);
                output(l,k,m,j) = output(j,k,l,m);
                output(k,j,m,l) = output(j,k,l,m);
                output(j,l,m,k) = output(j,k,l,m);
              }

        return output;
      }

};

#endif // DUNE_VISU_STATISTICS_HH
