#ifndef DUNE_VISU_FLOWMAP_HH
#define DUNE_VISU_FLOWMAP_HH

#include<dune/common/timer.hh>

#include<dune/visu/rungekutta.hh>
#include<dune/visu/vtkreader.hh>

/*
 * a map that advects particles based on a given vector field
 */
template<template<typename,unsigned int> typename Vector, typename VectorField, typename Range, unsigned int dim>
class FlowMap
{
  protected:

    const VectorField& vectorField;
    const PointGrid<Vector<Range,dim>,Range,dim>& preSet;
    PointSet<Vector<Range,dim>,dim> postSet;
    Range preTime, postTime, timeResolution;
    const unsigned int dataPartNumber;
    const unsigned int dataParts;
    RungeKutta<Vector,Range,dim> rungeKutta;

  public:

    FlowMap(const VectorField& vectorField_, const PointGrid<Vector<Range,dim>,Range,dim>& preSet_, const Range& preTime_, const Range& timeResolution_, const unsigned int dataPartNumber_ = 0, const unsigned int dataParts_ = 1)
      : vectorField(vectorField_), preSet(preSet_), postSet(preSet_.size(),dataPartNumber_,dataParts_), preTime(preTime_), postTime(preTime_), timeResolution(timeResolution_), dataPartNumber(dataPartNumber_), dataParts(dataParts_)
    {}

    void setPostTime(const Range& postTime_)
    {
      Dune::Timer timer(false);
      timer.start();

      if (std::abs(postTime_ - postTime) > 1e-10)
      {
        postTime = postTime_;

        const unsigned int substeps = std::abs(postTime - preTime)/timeResolution;
        rungeKutta.step(vectorField,preSet,preTime,postTime - preTime,substeps,postSet,dataPartNumber,dataParts);

        unsigned int invalid = 0;
        unsigned int sliceSize = preSet.size()/dataParts;
        if (preSet.size() % dataParts != 0)
          sliceSize++;
        for (unsigned int i = dataPartNumber * sliceSize; i < (dataPartNumber+1) * sliceSize && i < preSet.size(); i++)
          if (!(postSet[i].isValid()))
            invalid++;
        std::cout << "FlowMap: invalid points " << invalid << " / " << preSet.size() << " (" << (double)invalid/(double)preSet.size()*100. << "%)" << std::endl;

      }

      timer.stop();
    }

    const Range& getPostTime() const
    {
      return postTime;
    }

    void readFromFile(const std::string& fileName, const Range& postTime_)
    {
      if (dataParts == 1)
      {
        const std::string flowMapFileName = fileName + "_flowMap.vtk";
        const std::string validFileName   = fileName + "_valid.vtk";

        VTKReader flowMapReader(flowMapFileName);
        VTKReader validReader  (validFileName);
        Vector<Range,dim> isValid;

        Dune::Timer readTimer(false);
        readTimer.start();
        for (unsigned int i = 0; i < preSet.size(); i++)
        {
          if ((i + 1) % (preSet.size() / 100) == 0)
          {
            unsigned int elapsed = readTimer.elapsed();
            unsigned int percent = (i + 1)/(preSet.size()/100);
            std::cout << percent << "%, elapsed: " << elapsed/60. << " min, eta: " << (100. - percent)/percent * (elapsed/60.) << " min" << std::endl;
          }

          flowMapReader.eval<Vector<Range,dim>,dim>(preSet[i],postSet[i]);
          validReader.eval<Vector<Range,dim>,dim>(preSet[i],isValid);
          if (isValid[0] < 0.5)
            postSet[i].makeInvalid();
          else
            postSet[i].makeValid();
        }
      }
      else
      {
        postSet.prepareNeighbors();

        {
          std::stringstream s;
          s << "_" << dataPartNumber;
          std::string part;
          s >> part;
          const std::string flowMapFileName = fileName + "_flowMap.vtk" + part;
          const std::string validFileName   = fileName + "_valid.vtk" + part;

          std::ifstream flowMapFile(flowMapFileName);
          std::ifstream validFile(validFileName);

          Dune::Timer readTimer(false);
          readTimer.start();

          std::array<Range,3> line;
          double valid;
          unsigned int sliceSize = preSet.size()/dataParts;
          if (preSet.size() % dataParts != 0)
            sliceSize++;
          unsigned int i = dataPartNumber * sliceSize;
          while (flowMapFile >> line[0] >> line[1] >> line[2], validFile >> valid)
          {
            for (unsigned int j = 0; j < dim; j++)
              postSet[i][j] = line[j];

            if (valid > 0.5)
              postSet[i].makeValid();
            else
              postSet[i].makeInvalid();
            i++;
          }

          flowMapFile.close();
          validFile.close();
        }

        if (dataPartNumber > 0)
        {
          std::stringstream s;
          s << "_" << dataPartNumber - 1;
          std::string part;
          s >> part;
          const std::string flowMapFileName = fileName + "_flowMap.vtk" + part;
          const std::string validFileName   = fileName + "_valid.vtk" + part;

          std::ifstream flowMapFile(flowMapFileName);
          std::ifstream validFile(validFileName);

          Dune::Timer readTimer(false);
          readTimer.start();

          std::array<Range,3> line;
          double valid;
          unsigned int sliceSize = preSet.size()/dataParts;
          if (preSet.size() % dataParts != 0)
            sliceSize++;
          unsigned int i = (dataPartNumber-1) * sliceSize;
          while (flowMapFile >> line[0] >> line[1] >> line[2], validFile >> valid)
          {
            for (unsigned int j = 0; j < dim; j++)
              postSet[i][j] = line[j];

            if (valid > 0.5)
              postSet[i].makeValid();
            else
              postSet[i].makeInvalid();
            i++;
          }

          flowMapFile.close();
          validFile.close();
        }

        if (dataPartNumber < dataParts-1)
        {
          std::stringstream s;
          s << "_" << dataPartNumber + 1;
          std::string part;
          s >> part;
          const std::string flowMapFileName = fileName + "_flowMap.vtk" + part;
          const std::string validFileName   = fileName + "_valid.vtk" + part;

          std::ifstream flowMapFile(flowMapFileName);
          std::ifstream validFile(validFileName);

          Dune::Timer readTimer(false);
          readTimer.start();

          std::array<Range,3> line;
          double valid;
          unsigned int sliceSize = preSet.size()/dataParts;
          if (preSet.size() % dataParts != 0)
            sliceSize++;
          unsigned int i = (dataPartNumber+1) * sliceSize;
          while (flowMapFile >> line[0] >> line[1] >> line[2], validFile >> valid)
          {
            for (unsigned int j = 0; j < dim; j++)
              postSet[i][j] = line[j];

            if (valid > 0.5)
              postSet[i].makeValid();
            else
              postSet[i].makeInvalid();
            i++;
          }

          flowMapFile.close();
          validFile.close();
        }
      }

      postTime = postTime_;
    }

    const PointGrid<Vector<Range,dim>,Range,dim>& pre() const
    {
      return preSet;
    }

    const PointSet<Vector<Range,dim>,dim>& post() const
    {
      return postSet;
    }

};

/**
 * a flow map with added weights for the advected particles
 */
template<template<typename,unsigned int> typename Vector, typename VectorField, typename Range, unsigned int dim>
class WeightedFlowMap
: public FlowMap<Vector,VectorField,Range,dim>
{
  private:

    std::vector<std::pair<unsigned int, Range> > weightedIndicesVector;

  public:

    WeightedFlowMap(const VectorField& vectorField_, const PointGrid<Vector<Range,dim>,Range,dim>& preSet_, const Range& preTime_, const Range& timeResolution_, const unsigned int dataPartNumber = 0, const unsigned int dataParts = 1)
      : FlowMap<Vector,VectorField,Range,dim>::FlowMap(vectorField_,preSet_,preTime_,timeResolution_,dataPartNumber,dataParts)
    {}

    template<typename ProbDensity>
      void setWeights(ProbDensity& probDensity, const unsigned int meanIndex, const unsigned int indexWidth)
      {
        weightedIndicesVector.clear();
        Range totalWeight = 0.;

        probDensity.setMean(FlowMap<Vector,VectorField,Range,dim>::preSet[meanIndex]);
        const std::array<unsigned int,dim>& intervals = FlowMap<Vector,VectorField,Range,dim>::preSet.gridIntervals();
        std::array<unsigned int,dim> meanIndices;
        FlowMap<Vector,VectorField,Range,dim>::preSet.indexToIndices(meanIndex,meanIndices);
        std::array<unsigned int,dim> indices = meanIndices;
        unsigned int index;
        Range weight;

        if (dim == 2)
        {
          for (unsigned int i = 0; i < 2*indexWidth; i++)
          {
            indices[0] = meanIndices[0] + i - indexWidth;
            for (unsigned int j = 0; j < 2*indexWidth; j++)
            {
              indices[1] = meanIndices[1] + j - indexWidth;

              if (indices[0] < intervals[0]+1 && indices[1] < intervals[1]+1)
              {
                FlowMap<Vector,VectorField,Range,dim>::preSet.indicesToIndex(indices,index);
                weight = FlowMap<Vector,VectorField,Range,dim>::preSet.cellVolume() * probDensity(FlowMap<Vector,VectorField,Range,dim>::preSet[index]);
                weightedIndicesVector.push_back(std::pair<unsigned int, Range>(index,weight));
                totalWeight += weight;
              }
            }
          }
        }
        else if (dim == 3)
        {
          for (unsigned int i = 0; i < 2*indexWidth; i++)
          {
            indices[0] = meanIndices[0] + i - indexWidth;
            for (unsigned int j = 0; j < 2*indexWidth; j++)
            {
              indices[1] = meanIndices[1] + j - indexWidth;
              for (unsigned int k = 0; k < 2*indexWidth; k++)
              {
                indices[2] = meanIndices[2] + k - indexWidth;

                if (indices[0] < intervals[0]+1 && indices[1] < intervals[1]+1 && indices[2] < intervals[2]+1)
                {
                  FlowMap<Vector,VectorField,Range,dim>::preSet.indicesToIndex(indices,index);
                  weight = FlowMap<Vector,VectorField,Range,dim>::preSet.cellVolume() * probDensity(FlowMap<Vector,VectorField,Range,dim>::preSet[index]);
                  weightedIndicesVector.push_back(std::pair<unsigned int, Range>(index,weight));
                  totalWeight += weight;
                }
              }
            }
          }
        }

        for (unsigned int i = 0; i < weightedIndicesVector.size(); i++)
          weightedIndicesVector[i].second /= totalWeight;
      }

    template<typename ProbDensity>
      void setWeights(ProbDensity& probDensity, const Vector<Range,dim>& mean, const unsigned int indexWidth)
      {
        std::vector<std::pair<unsigned int,Range> > fullWeightedIndices;
        std::vector<unsigned int> meanIndices;
        std::vector<Range>        meanWeight;
        Range totalWeight = 0.;
        FlowMap<Vector,VectorField,Range,dim>::preSet.interpolate(mean,meanIndices,meanWeight);
        for (unsigned int i = 0; i < meanIndices.size(); i++)
        {
          if (meanWeight[i] > 1e-6)
          {
            setWeights(probDensity,meanIndices[i],indexWidth);
            for (unsigned int j = 0; j < weightedIndicesVector.size(); j++)
            {
              if (!std::isfinite(fullWeightedIndices.back().second))
              {
                std::cout << "Vector: " << mean << " index: " << meanIndices[i] << " weight: " << meanWeight[i] << " gridPoint: " << fullWeightedIndices.back().first << " pointWeight: " << fullWeightedIndices.back().second << " meanWeight: " << meanWeight[i] << " indexWeight: " << weightedIndicesVector[j].second << std::endl;
              }
              else
              {
                fullWeightedIndices.push_back(std::pair<unsigned int,Range>(weightedIndicesVector[j].first,weightedIndicesVector[j].second * meanWeight[i]));
                totalWeight += weightedIndicesVector[j].second * meanWeight[i];
              }
            }
          }
        }
        for (unsigned int i = 0; i < fullWeightedIndices.size(); i++)
          fullWeightedIndices[i].second /= totalWeight;

        weightedIndicesVector = fullWeightedIndices;
      }

    const std::vector<std::pair<unsigned int, Range> >& weightedIndices() const
    {
      return weightedIndicesVector;
    }

};

#endif // DUNE_VISU_FLOWMAP_HH
