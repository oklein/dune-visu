#ifndef DUNE_VISU_VECTOR_HH
#define DUNE_VISU_VECTOR_HH

#include<array>
#include<iostream>

/**
 * class for vectors and points in space
 */
template<typename Range, unsigned int dim>
class CartesianVector
{
  private:

    std::array<Range,dim> coords;
    bool valid;

  public:

    CartesianVector()
      : valid(true)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] = 0.;
    }

    CartesianVector(const std::array<Range,dim>& coords_)
      : coords(coords_), valid(true)
    {}

    Range& operator[](const unsigned int i)
    {
      return coords[i];
    }

    const Range& operator[](const unsigned int i) const
    {
      return coords[i];
    }

    void makeValid()
    {
      valid = true;
    }

    void makeInvalid()
    {
      valid = false;
    }

    bool isValid() const
    {
      return valid;
    }

    CartesianVector& operator+=(const CartesianVector& other)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] += other.coords[i];

      return *this;
    }

    CartesianVector& operator-=(const CartesianVector& other)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] -= other.coords[i];

      return *this;
    }

    CartesianVector& operator*=(const Range& alpha)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] *= alpha;

      return *this;
    }

    CartesianVector& axpy(const Range& alpha, const CartesianVector& other)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] += alpha * other[i];

      return *this;
    }

    CartesianVector operator+(const CartesianVector& other) const
    {
      CartesianVector output = *this;
      output += other;

      return output;
    }

    CartesianVector operator-(const CartesianVector& other) const
    {
      CartesianVector output = *this;
      output -= other;

      return output;
    }

    Range operator*(const CartesianVector& other) const
    {
      Range output = 0.;
      for (unsigned int i = 0; i < dim; i++)
        output += coords[i] * other.coords[i];

      return output;
    }

    Range maxNorm() const
    {
      Range output = 0.;
      for (unsigned int i = 0; i < dim; i++)
        if (output < std::abs(coords[i]))
          output = coords[i];

      return output;
    }

    Range twoNorm() const
    {
      Range output = 0.;
      for (unsigned int i = 0; i < dim; i++)
        output += coords[i]*coords[i];

      output = std::sqrt(output);

      return output;
    }

    void normalize(int& timesLon, int& timesLat)
    {
    }

    void switchSign(const int& timesLon, const int& timesLat)
    {
    }

};

  template<typename Range, unsigned int dim>
CartesianVector<Range,dim> operator+(const CartesianVector<Range,dim>& first, const CartesianVector<Range,dim>& second)
{
  CartesianVector<Range,dim> output = first;
  output += second;

  return output;
}

  template<typename Range, unsigned int dim>
CartesianVector<Range,dim> operator-(const CartesianVector<Range,dim>& first, const CartesianVector<Range,dim>& second)
{
  CartesianVector<Range,dim> output = first;
  output -= second;

  return output;
}

  template<typename Range, unsigned int dim>
CartesianVector<Range,dim> operator*(const Range& alpha, const CartesianVector<Range,dim>& vector)
{
  CartesianVector<Range,dim> output = vector;
  output *= alpha;

  return output;
}

  template<typename Range, unsigned int dim>
std::ostream& operator<<(std::ostream& out, const CartesianVector<Range,dim>& vector)
{
  for (unsigned int i = 0; i < dim - 1; i++)
    out << vector[i] << " ";
  out << vector[dim-1];

  return out;
}

/**
 * class for vectors and points in space, first two dimensions angular
 */
template<typename Range, unsigned int dim>
class SphericalVector
{
  private:

    std::array<Range,dim> coords;
    bool valid;

  public:

    SphericalVector()
      : valid(true)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] = 0.;
    }

    SphericalVector(const std::array<Range,dim>& coords_)
      : coords(coords_), valid(true)
    {}

    Range& operator[](const unsigned int i)
    {
      return coords[i];
    }

    const Range& operator[](const unsigned int i) const
    {
      return coords[i];
    }

    void makeValid()
    {
      valid = true;
    }

    void makeInvalid()
    {
      valid = false;
    }

    bool isValid() const
    {
      return valid;
    }

    SphericalVector& operator+=(const SphericalVector& other)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] += other.coords[i];

      return *this;
    }

    SphericalVector& operator-=(const SphericalVector& other)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] -= other.coords[i];

      return *this;
    }

    SphericalVector& operator*=(const Range& alpha)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] *= alpha;

      return *this;
    }

    SphericalVector& axpy(const Range& alpha, const SphericalVector& other)
    {
      for (unsigned int i = 0; i < dim; i++)
        coords[i] += alpha * other[i];

      return *this;
    }

    SphericalVector operator+(const SphericalVector& other) const
    {
      SphericalVector output = *this;
      output += other;

      return output;
    }

    SphericalVector operator-(const SphericalVector& other) const
    {
      SphericalVector output = *this;
      output -= other;

      return output;
    }

    Range operator*(const SphericalVector& other) const
    {
      Range output = 0.;
      for (unsigned int i = 0; i < dim; i++)
        output += coords[i] * other.coords[i];

      return output;
    }

    Range maxNorm() const
    {
      Range output = 0.;
      for (unsigned int i = 0; i < dim; i++)
        if (output < std::abs(coords[i]))
          output = coords[i];

      return output;
    }

    Range twoNorm() const
    {
      Range output = 0.;
      for (unsigned int i = 0; i < dim; i++)
        output += coords[i]*coords[i];

      output = std::sqrt(output);

      return output;
    }

    void normalize(int& timesLon, int& timesLat)
    {
      const Range eps = 1e-6;

      timesLat = std::floor((coords[1] + M_PI/2.)/M_PI);
      coords[1] -= timesLat * M_PI;
      switchPos(timesLon,timesLat);

      if (coords[1] >= M_PI/2. - 0.01)
        coords[1] = M_PI/2. - 0.01 - eps;
      else if (coords[1] <= -M_PI/2. + 0.01)
        coords[1] = -M_PI/2. + 0.01 + eps;

      timesLon = std::floor((coords[0] + M_PI)/(2.*M_PI));
      coords[0] -= timesLon * (2.*M_PI);

      if (coords[0] >= M_PI - 0.01)
        coords[0] = M_PI - 0.01 - eps;
      else if (coords[0] <= -M_PI + 0.001)
        coords[0] = -M_PI + 0.001 + eps;
    }

    void switchPos(const int& timesLon, const int& timesLat)
    {
      if (timesLat % 2 != 0)
      {
        coords[0] = M_PI + coords[0];
        coords[1] = - coords[1];
      }
    }

    void switchSign(const int& timesLon, const int& timesLat)
    {
      if (timesLat % 2 != 0)
        coords[1] = - coords[1];
    }

};

  template<typename Range, unsigned int dim>
SphericalVector<Range,dim> operator+(const SphericalVector<Range,dim>& first, const SphericalVector<Range,dim>& second)
{
  SphericalVector<Range,dim> output = first;
  output += second;

  return output;
}

  template<typename Range, unsigned int dim>
SphericalVector<Range,dim> operator-(const SphericalVector<Range,dim>& first, const SphericalVector<Range,dim>& second)
{
  SphericalVector<Range,dim> output = first;
  output -= second;

  return output;
}

  template<typename Range, unsigned int dim>
SphericalVector<Range,dim> operator*(const Range& alpha, const SphericalVector<Range,dim>& vector)
{
  SphericalVector<Range,dim> output = vector;
  output *= alpha;

  return output;
}

  template<typename Range, unsigned int dim>
std::ostream& operator<<(std::ostream& out, const SphericalVector<Range,dim>& vector)
{
  for (unsigned int i = 0; i < dim - 1; i++)
    out << vector[i] << " ";
  out << vector[dim-1];

  return out;
}

#endif // DUNE_VISU_VECTOR_HH
