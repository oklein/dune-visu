#ifndef DUNE_VISU_MATRIX_HH
#define DUNE_VISU_MATRIX_HH

#include<eigen3/Eigen/Dense>
#include<eigen3/Eigen/SVD>

template<template<typename,unsigned int> typename Vector, typename Range, unsigned int dim>
class Matrix
{
  private:

    Eigen::Matrix<Range,dim,dim> storage;
    mutable Eigen::SelfAdjointEigenSolver<Eigen::Matrix<Range,dim,dim> > solver;
    mutable Eigen::JacobiSVD<Eigen::Matrix<Range,dim,dim> > jacobiSVD;
    mutable bool eigenValid;
    mutable bool singularValid;

  public:

    Matrix()
      : eigenValid(false), singularValid(false)
    {
      for (unsigned int i = 0; i < dim; i++)
        for (unsigned int j = 0; j < dim; j++)
          storage(i,j) = 0;
    }

    Range& operator()(unsigned int i, unsigned int j)
    {
      return storage(i,j);
    }

    const Range& operator()(unsigned int i, unsigned int j) const
    {
      return storage(i,j);
    }

    void computeEigen() const
    {
      solver.compute(storage);
      eigenValid = true;
    }

    void computeSingular() const
    {
      jacobiSVD.compute(storage);
      singularValid = true;
    }

    Vector<Range,dim> eigen() const
    {
      Vector<Range,dim> output;

      if (!eigenValid)
        computeEigen();

      for (unsigned int i = 0; i < dim; i++)
        output[i] = solver.eigenvalues()[i];

      return output;
    }

    Vector<Range,dim> singular() const
    {
      Vector<Range,dim> output;

      if (!singularValid)
        jacobiSVD.compute(storage);

      for (unsigned int i = 0; i < dim; i++)
        output[i] = jacobiSVD.singularValues()[i];

      return output;
    }

    Range spectralNorm() const
    {
      Vector<Range,dim> eigenVector = eigen();
      Range output = 0.;

      for (unsigned int i = 0; i < dim; i++)
        if (output < eigenVector[i])
          output = eigenVector[i];

      return output;
    }

    template<unsigned int reducedDim>
      Range partialSpectralNorm() const
      {
        Matrix<Vector,Range,reducedDim> reducedMatrix;
        for (unsigned int i = 0; i < reducedDim; i++)
          for (unsigned int j = 0; j < reducedDim; j++)
            reducedMatrix(i,j) = operator()(i,j);

        Vector<Range,reducedDim> eigenVector = reducedMatrix.eigen();
        Range output = 0.;

        for (unsigned int i = 0; i < reducedDim; i++)
          if (output < eigenVector[i])
            output = eigenVector[i];

        return output;
      }

    Range nuclearNorm() const
    {
      Vector<Range,dim> eigenVector = eigen();
      Range output = 0.;

      for (unsigned int i = 0; i < dim; i++)
        output += eigenVector[i];

      return output;
    }

    Range frobeniusNorm() const
    {
      Range output = 0.;

      for (unsigned int i = 0; i < dim; i++)
        for (unsigned int j = 0; j < dim; j++)
          output += storage(i,j)*storage(i,j);

      output = std::sqrt(output);

      return output;
    }

    Range determinant() const
    {
      Vector<Range,dim> eigenVector = eigen();
      Range output = 1.;

      for (unsigned int i = 0; i < dim; i++)
        output *= eigenVector[i];

      return output;
    }

    void timesRootInverse(Vector<Range,dim>& vector) const
    {
      Vector<Range,dim> output;

      std::array<Vector<Range,dim>,dim> eigenvectors;
      std::array<Range,dim> eigenvalues;

      if (!eigenValid)
        computeEigen();

      for (unsigned int i = 0; i < dim; i++)
      {
        eigenvalues[i] = solver.eigenvalues()[i];
        for (unsigned int j = 0; j < dim; j++)
        {
          eigenvectors[i][j] = solver.eigenvectors().col(i)[j];
        }
      }

      for (unsigned int i = 0; i < dim; i++)
        output.axpy((eigenvectors[i]*vector) / std::sqrt(eigenvalues[i]),eigenvectors[i]);

      vector = output;
    }
};

#endif // DUNE_VISU_MATRIX_HH
