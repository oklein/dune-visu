#ifndef DUNE_VISU_POINTSET_HH
#define DUNE_VISU_POINTSET_HH

#include<vector>
#include<iostream>

/**
 * a finite set of points in the domain
 */
template<typename Vector, unsigned int dim>
class PointSet
{
  private:

    const unsigned int dataPartNumber;
    const unsigned int fullSize;
    const unsigned int sliceSize;
    std::vector<Vector> points;
    std::vector<Vector> upperNeighbor, lowerNeighbor;

  public:

    PointSet(const unsigned int size_, const unsigned int dataPartNumber_, const unsigned int dataParts)
      : dataPartNumber(dataPartNumber_), fullSize(size_), sliceSize(size_/dataParts+((size_%dataParts==0)?0:1)), points(sliceSize)
      {}

    unsigned int size() const
    {
      return points.size();
    }

    void prepareNeighbors()
    {
      upperNeighbor.resize(points.size());
      lowerNeighbor.resize(points.size());
    }

    Vector& operator[](unsigned int i)
    {
      const unsigned int index = i - dataPartNumber*sliceSize;
      if (index >= 2*sliceSize)
        return lowerNeighbor[i - (dataPartNumber-1)*sliceSize];
      if (index >= sliceSize)
        return upperNeighbor[i - (dataPartNumber+1)*sliceSize];

      return points[i - dataPartNumber*sliceSize];
    }

    const Vector& operator[](unsigned int i) const
    {
      const unsigned int index = i - dataPartNumber*sliceSize;
      if (index >= 2*sliceSize)
        return lowerNeighbor[i - (dataPartNumber-1)*sliceSize];
      if (index >= sliceSize)
        return upperNeighbor[i - (dataPartNumber+1)*sliceSize];

      return points[i - dataPartNumber*sliceSize];
    }

};

/**
 * set of points with regular placement
 */
template<typename Vector, typename Range, unsigned int dim>
class PointGrid
{
  private:

    Range volume;
    const Vector origin;
    const Vector extent;
    const Vector start;
    Vector stride;
    const std::array<unsigned int,dim>& intervals;

  public:

    PointGrid(const Vector& origin_, const Vector& extent_, const std::array<unsigned int,dim>& intervals_)
      : origin(origin_), extent(extent_), start(origin - 0.5*extent), intervals(intervals_)
    {
      volume = 1.;
      stride = extent;
      for (unsigned int i = 0; i < dim; i++)
      {
        stride[i] /= intervals[i];
        volume *= stride[i];
      }
    }

    unsigned int size() const
    {
      unsigned int output = 1;
      for (unsigned int i = 0; i < dim; i++)
        output *= intervals[i] + 1;

      return output;
    }

    Vector operator[](unsigned int i) const
    {
      Vector point = start;

      std::array<unsigned int,dim> indices;
      indexToIndices(i,indices);

      for (unsigned int j = 0; j < dim; j++)
        point[j] += indices[j] * stride[j];

      return point;
    }

    Range cellVolume() const
    {
      return volume;
    }

    const Vector& gridOrigin() const
    {
      return origin;
    }

    const Vector& gridExtent() const
    {
      return extent;
    }

    const std::array<unsigned int,dim>& gridIntervals() const
    {
      return intervals;
    }

    void indexToIndices(const unsigned int& index, std::array<unsigned int,dim>& indices) const
    {
      if (dim == 2)
      {
        indices[0] =  index % (intervals[0]+1);
        indices[1] = (index / (intervals[0]+1)) % (intervals[1]+1);
      }
      else if (dim == 3)
      {
        indices[0] =  index %  (intervals[0]+1);
        indices[1] = (index /  (intervals[0]+1)) % (intervals[1]+1);
        indices[2] = (index / ((intervals[0]+1)  * (intervals[1]+1))) % (intervals[2]+1);
      }
    }

    void indicesToIndex(const std::array<unsigned int,dim>& indices, unsigned int& index) const
    {
      if (dim == 2)
      {
        index = indices[0] + (intervals[0]+1) * indices[1];
      }
      else if (dim == 3)
      {
        index = indices[0] + (intervals[0]+1) * indices[1] + (intervals[0]+1)*(intervals[1]+1) * indices[2];
      }
    }

    void interpolate(const Vector& point, std::vector<unsigned int>& indices, std::vector<Range>& weights) const
    {
      indices.clear();
      weights.clear();

      std::array<unsigned int,dim> pointIndices;
      std::array<double,dim> pointWeights;
      unsigned int index;
      std::array<double,dim> stride;
      for (unsigned int i = 0; i < dim; i++)
      {
        stride[i]       = extent[i] / intervals[i];
        pointIndices[i] = (point[i] - (origin[i] - 0.5*extent[i])) / stride[i];
        pointWeights[i] = (point[i] - ((origin[i] - 0.5*extent[i]) + pointIndices[i]*stride[i]))/stride[i];
      }

      std::array<unsigned int,dim> cornerIndices = pointIndices;
      std::array<Range,dim>        cornerWeights = pointWeights;
      if (dim == 2)
      {
        for (unsigned int i = 0; i < 4; i++)
        {
          if (i % 2 == 0)
          {
            cornerWeights[0] = 1. - pointWeights[0];
            cornerIndices[0] = pointIndices[0] + 1;
          }
          else
          {
            cornerWeights[0] = pointWeights[0];
            cornerIndices[0] = pointIndices[0];
          }

          if ((i/2) % 2 == 0)
          {
            cornerWeights[1] = 1. - pointWeights[1];
            cornerIndices[1] = pointIndices[1] + 1;
          }
          else
          {
            cornerWeights[1] = pointWeights[1];
            cornerIndices[1] = pointIndices[1];
          }

          if (cornerIndices[0] < intervals[0]+1 && cornerIndices[1] < intervals[1]+1)
          {
            indicesToIndex(cornerIndices,index);
            indices.push_back(index);
            weights.push_back(cornerWeights[0]*cornerWeights[1]);
          }
        }
      }
      else if (dim == 3)
      {
        for (unsigned int i = 0; i < 8; i++)
        {
          if (i % 2 == 0)
          {
            cornerWeights[0] = 1. - pointWeights[0];
            cornerIndices[0] = pointIndices[0] + 1;
          }
          else
          {
            cornerWeights[0] = pointWeights[0];
            cornerIndices[0] = pointIndices[0];
          }

          if ((i/2) % 2 == 0)
          {
            cornerWeights[1] = 1. - pointWeights[1];
            cornerIndices[1] = pointIndices[1] + 1;
          }
          else
          {
            cornerWeights[1] = pointWeights[1];
            cornerIndices[1] = pointIndices[1];
          }

          if ((i/4) % 2 == 0)
          {
            cornerWeights[2] = 1. - pointWeights[2];
            cornerIndices[2] = pointIndices[2] + 1;
          }
          else
          {
            cornerWeights[2] = pointWeights[2];
            cornerIndices[2] = pointIndices[2];
          }

          if (cornerIndices[0] < intervals[0]+1 && cornerIndices[1] < intervals[1]+1 && cornerIndices[2] < intervals[2]+1)
          {
            indicesToIndex(cornerIndices,index);
            indices.push_back(index);
            weights.push_back(cornerWeights[0]*cornerWeights[1]*cornerWeights[2]);
          }
        }
      }

      Range totalWeight = 0.;
      for (unsigned int i = 0; i < indices.size(); i++)
        totalWeight += weights[i];
      for (unsigned int i = 0; i < indices.size(); i++)
        weights[i] /= totalWeight;

      for (unsigned int i = 0; i < indices.size(); i++)
        if (!std::isfinite(weights[i]))
        {
          indices.clear();
          weights.clear();
        }
    }

};

#endif // DUNE_VISU_POINTSET_HH
