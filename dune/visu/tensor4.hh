#ifndef DUNE_VISU_TENSOR4_HH
#define DUNE_VISU_TENSOR4_HH

template<typename Range, unsigned int dim>
class Tensor4
{
  private:

    std::array<Range,dim*dim*dim*dim> storage;

  public:

    Tensor4()
    {
      for (unsigned int i = 0; i < dim; i++)
        for (unsigned int j = 0; j < dim; j++)
          for (unsigned int k = 0; k < dim; k++)
            for (unsigned int l = 0; l < dim; l++)
              storage[i + j*dim + k*dim*dim + l*dim*dim*dim] = 0;
    }

    Range& operator()(unsigned int i, unsigned int j, unsigned int k, unsigned int l)
    {
      return storage[i + j*dim + k*dim*dim + l*dim*dim*dim];
    }

    const Range& operator()(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const
    {
      return storage[i + j*dim + k*dim*dim + l*dim*dim*dim];
    }

    Range hilbertSchmidtNorm() const
    {
      Range output = 0.;

      for (unsigned int i = 0; i < dim; i++)
        for (unsigned int j = 0; j < dim; j++)
          for (unsigned int k = 0; k < dim; k++)
            for (unsigned int l = 0; l < dim; l++)
              output += storage[i + j*dim + k*dim*dim + l*dim*dim*dim]*storage[i + j*dim + k*dim*dim + l*dim*dim*dim];

      output = std::sqrt(output);

      return output;
    }

};

#endif // DUNE_VISU_TENSOR4_HH
