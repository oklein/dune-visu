#ifndef DUNE_VISU_TENSOR3_HH
#define DUNE_VISU_TENSOR3_HH

//#include<eigen3/Eigen/Dense>
//#include<unsupported/Eigen/CXX11/Tensor>

template<typename Range, unsigned int dim>
class Tensor3
{
  private:

    std::array<Range,dim*dim*dim> storage;

  public:

    Tensor3()
    {
      for (unsigned int i = 0; i < dim; i++)
        for (unsigned int j = 0; j < dim; j++)
          for (unsigned int k = 0; k < dim; k++)
            storage[i + j*dim + k*dim*dim] = 0;
    }

    Range& operator()(unsigned int i, unsigned int j, unsigned int k)
    {
      return storage[i + j*dim + k*dim*dim];
    }

    const Range& operator()(unsigned int i, unsigned int j, unsigned int k) const
    {
      return storage[i + j*dim + k*dim*dim];
    }

    Range hilbertSchmidtNorm() const
    {
      Range output = 0.;

      for (unsigned int i = 0; i < dim; i++)
        for (unsigned int j = 0; j < dim; j++)
          for (unsigned int k = 0; k < dim; k++)
            output += storage[i + j*dim + k*dim*dim]*storage[i + j*dim + k*dim*dim];

      output = std::sqrt(output);

      return output;
    }

};

#endif // DUNE_VISU_TENSOR3_HH
