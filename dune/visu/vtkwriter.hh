#ifndef DUNE_VISU_VTKWRITER
#define DUNE_VISU_VTKWRITER

class VTKWriter
{
  public:

    VTKWriter()
    {}

    template<typename PointGrid, typename DataVector>
      void writeScalarData(const std::string& fileName, const std::string& dataName, const PointGrid& pointGrid, const DataVector& data, const unsigned int dataPartNumber = 0, const int dataParts = 1)
      {
        if (dataParts == 1)
        {
          std::string extendedFileName;
          std::stringstream s;
          s << fileName;
          s >> extendedFileName;

          std::ofstream file(extendedFileName,std::ofstream::trunc);

          file << "# vtk DataFile Version 2.0" << std::endl
            << dataName << std::endl
            << "ASCII" << std::endl
            << "DATASET STRUCTURED_POINTS" << std::endl;
          file << "DIMENSIONS " << pointGrid.gridIntervals()[0]+1 << " "
            << pointGrid.gridIntervals()[1]+1 << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << pointGrid.gridIntervals()[2]+1;
          else
            file << 1;
          file << std::endl;
          file << "ASPECT_RATIO " << pointGrid.gridExtent()[0]/pointGrid.gridIntervals()[0] << " "
            << pointGrid.gridExtent()[1]/pointGrid.gridIntervals()[1] << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << pointGrid.gridExtent()[2]/pointGrid.gridIntervals()[2];
          else
            file << 1;
          file << std::endl;
          file << "ORIGIN " << pointGrid.gridOrigin()[0] - 0.5*pointGrid.gridExtent()[0] << " "
            << pointGrid.gridOrigin()[1] - 0.5*pointGrid.gridExtent()[1] << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << pointGrid.gridOrigin()[2] - 0.5*pointGrid.gridExtent()[2];
          else
            file << 0;
          file << std::endl;
          file << "POINT_DATA " << pointGrid.size() << std::endl
            << "SCALARS " << dataName << " float 1" << std::endl
            << "LOOKUP_TABLE default"         << std::endl;

          std::cout << "pointGrid.size: " << pointGrid.size() << " data.size: " << data.size() << std::endl;

          unsigned int sliceSize = pointGrid.size()/dataParts;
          if (pointGrid.size() % dataParts != 0)
            sliceSize++;
          for (unsigned int i = 0; i < data.size(); i++)
            file << data[i] << std::endl;

          file.close();
        }
        else
        {
          std::string extendedFileName;
          std::stringstream s;
          s << fileName;
          if (dataParts > 1)
            s << "_" << dataPartNumber;
          s >> extendedFileName;

          std::ofstream file(extendedFileName,std::ofstream::trunc);

          if (dataPartNumber == 0)
          {
            std::ofstream headerFile(fileName+"_header",std::ofstream::trunc);

            headerFile << "# vtk DataFile Version 2.0" << std::endl
              << dataName << std::endl
              << "ASCII" << std::endl
              << "DATASET STRUCTURED_POINTS" << std::endl;
            headerFile << "DIMENSIONS " << pointGrid.gridIntervals()[0]+1 << " "
              << pointGrid.gridIntervals()[1]+1 << " ";
            if (pointGrid.gridIntervals().size() == 3)
              headerFile << pointGrid.gridIntervals()[2]+1;
            else
              headerFile << 1;
            headerFile << std::endl;
            headerFile << "ASPECT_RATIO " << pointGrid.gridExtent()[0]/pointGrid.gridIntervals()[0] << " "
              << pointGrid.gridExtent()[1]/pointGrid.gridIntervals()[1] << " ";
            if (pointGrid.gridIntervals().size() == 3)
              headerFile << pointGrid.gridExtent()[2]/pointGrid.gridIntervals()[2];
            else
              headerFile << 1;
            headerFile << std::endl;
            headerFile << "ORIGIN " << pointGrid.gridOrigin()[0] - 0.5*pointGrid.gridExtent()[0] << " "
              << pointGrid.gridOrigin()[1] - 0.5*pointGrid.gridExtent()[1] << " ";
            if (pointGrid.gridIntervals().size() == 3)
              headerFile << pointGrid.gridOrigin()[2] - 0.5*pointGrid.gridExtent()[2];
            else
              headerFile << 0;
            headerFile << std::endl;
            headerFile << "POINT_DATA " << pointGrid.size() << std::endl
              << "SCALARS " << dataName << " float 1" << std::endl
              << "LOOKUP_TABLE default"         << std::endl;

            headerFile.close();
          }

          std::cout << "pointGrid.size: " << pointGrid.size() << " data.size: " << data.size() << std::endl;

          unsigned int sliceSize = pointGrid.size()/dataParts;
          if (pointGrid.size() % dataParts != 0)
            sliceSize++;
          for (unsigned int i = 0; i < data.size(); i++)
            file << data[i] << std::endl;

          file.close();
        }
      }

    template<typename PointGrid, typename DataVector>
      void writeVectorData(const std::string& fileName, const std::string& dataName, const PointGrid& pointGrid, const DataVector& data, const unsigned int dataPartNumber = 0, const unsigned int dataParts = 1)
      {
        if (dataParts == 1)
        {
          std::string extendedFileName;
          std::stringstream s;
          s << fileName;
          s >> extendedFileName;

          std::ofstream file(extendedFileName,std::ofstream::trunc);

          file << "# vtk DataFile Version 2.0" << std::endl
            << dataName << std::endl
            << "ASCII" << std::endl
            << "DATASET STRUCTURED_POINTS" << std::endl;
          file << "DIMENSIONS " << pointGrid.gridIntervals()[0]+1 << " "
            << pointGrid.gridIntervals()[1]+1 << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << pointGrid.gridIntervals()[2]+1;
          else
            file << 1;
          file << std::endl;
          file << "ASPECT_RATIO " << pointGrid.gridExtent()[0]/pointGrid.gridIntervals()[0] << " "
            << pointGrid.gridExtent()[1]/pointGrid.gridIntervals()[1] << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << pointGrid.gridExtent()[2]/pointGrid.gridIntervals()[2];
          else
            file << 1;
          file << std::endl;
          file << "ORIGIN " << pointGrid.gridOrigin()[0] - 0.5*pointGrid.gridExtent()[0] << " "
            << pointGrid.gridOrigin()[1] - 0.5*pointGrid.gridExtent()[1] << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << pointGrid.gridOrigin()[2] - 0.5*pointGrid.gridExtent()[2];
          else
            file << 0;
          file << std::endl;
          file << "POINT_DATA " << pointGrid.size() << std::endl
            << "VECTORS " << dataName << " float" << std::endl;

          std::cout << "pointGrid.size: " << pointGrid.size() << " data.size: " << data.size() << std::endl;

          if (pointGrid.gridIntervals().size() == 3)
          {
            unsigned int sliceSize = pointGrid.size()/dataParts;
            if (pointGrid.size() % dataParts != 0)
              sliceSize++;
            for (unsigned int i = 0; i < data.size(); i++)
              file << data[i] << std::endl;
          }
          else
          {
            unsigned int sliceSize = pointGrid.size()/dataParts;
            if (pointGrid.size() % dataParts != 0)
              sliceSize++;
            for (unsigned int i = 0; i < data.size(); i++)
              file << data[i] << " 0" << std::endl;
          }

          file.close();
        }
        else
        {
          std::string extendedFileName;
          std::stringstream s;
          s << fileName;
          s << "_" << dataPartNumber;
          s >> extendedFileName;

          std::ofstream file(extendedFileName,std::ofstream::trunc);

          if (dataPartNumber == 0)
          {
            std::ofstream headerFile(fileName+"_header",std::ofstream::trunc);
            headerFile << "# vtk DataFile Version 2.0" << std::endl
              << dataName << std::endl
              << "ASCII" << std::endl
              << "DATASET STRUCTURED_POINTS" << std::endl;
            headerFile << "DIMENSIONS " << pointGrid.gridIntervals()[0]+1 << " "
              << pointGrid.gridIntervals()[1]+1 << " ";
            if (pointGrid.gridIntervals().size() == 3)
              headerFile << pointGrid.gridIntervals()[2]+1;
            else
              headerFile << 1;
            headerFile << std::endl;
            headerFile << "ASPECT_RATIO " << pointGrid.gridExtent()[0]/pointGrid.gridIntervals()[0] << " "
              << pointGrid.gridExtent()[1]/pointGrid.gridIntervals()[1] << " ";
            if (pointGrid.gridIntervals().size() == 3)
              headerFile << pointGrid.gridExtent()[2]/pointGrid.gridIntervals()[2];
            else
              headerFile << 1;
            headerFile << std::endl;
            headerFile << "ORIGIN " << pointGrid.gridOrigin()[0] - 0.5*pointGrid.gridExtent()[0] << " "
              << pointGrid.gridOrigin()[1] - 0.5*pointGrid.gridExtent()[1] << " ";
            if (pointGrid.gridIntervals().size() == 3)
              headerFile << pointGrid.gridOrigin()[2] - 0.5*pointGrid.gridExtent()[2];
            else
              headerFile << 0;
            headerFile << std::endl;
            headerFile << "POINT_DATA " << pointGrid.size() << std::endl
              << "VECTORS " << dataName << " float" << std::endl;

            headerFile.close();
          }

          std::cout << "pointGrid.size: " << pointGrid.size() << " data.size: " << data.size() << std::endl;

          if (pointGrid.gridIntervals().size() == 3)
          {
            unsigned int sliceSize = pointGrid.size()/dataParts;
            if (pointGrid.size() % dataParts != 0)
              sliceSize++;
            for (unsigned int i = 0; i < data.size(); i++)
              file << data[i] << std::endl;
          }
          else
          {
            unsigned int sliceSize = pointGrid.size()/dataParts;
            if (pointGrid.size() % dataParts != 0)
              sliceSize++;
            for (unsigned int i = 0; i < data.size(); i++)
              file << data[i] << " 0" << std::endl;
          }

          file.close();
        }
      }

    template<typename PointGrid, typename DataVector>
      void writeLineData(const std::string& fileName, const std::string& dataName, const PointGrid& pointGrid, const DataVector& data)
      {
        std::ofstream file(fileName,std::ofstream::trunc);

        file << "# vtk DataFile Version 2.0" << std::endl
          << dataName << std::endl
          << "ASCII" << std::endl
          << "DATASET POLYDATA" << std::endl;
        file << "POINTS " << data.size() << " float" << std::endl;
        for (unsigned int i = 0; i < data.size(); i++)
        {
          file << data[i][0] << " " << data[i][1] << " ";
          if (pointGrid.gridIntervals().size() == 3)
            file << data[i][2];
          else
            file << "0";
          file << std::endl;
        }
        file << "LINES " << data.size()-1 << " " << 3*(data.size()-1) << std::endl;
        for (unsigned int i = 0; i < data.size()-1; i++)
          file << "2 " << i << " " << i+1 << std::endl;

        file.close();
      }

};

#endif // DUNE_VISU_VTKWRITER
