#ifndef DUNE_VISU_ENSEMBLEPVDREADER
#define DUNE_VISU_ENSEMBLEPVDREADER

#include<fstream>
#include<sstream>
#include<string>
#include<regex>

#include<list>

#include<dune/visu/pvdreader.hh>

class ParamEnsemblePVDReader
{
  private:

    std::list<std::pair<double,PVDReader> > pvdReaders;
    double firstParam, lastParam;
    mutable double lowerParam, upperParam;
    mutable std::list<std::pair<double,PVDReader> >::const_iterator lowerData, upperData;

  public:

    ParamEnsemblePVDReader(const std::string& fileName, const std::array<bool,3>& periodic = {0,0,0}, int arrayId_ = 0, double cutoffValue_ = std::numeric_limits<double>::max())
      : firstParam(std::numeric_limits<double>::max()),
      lastParam(-std::numeric_limits<double>::max()),
      lowerParam(std::numeric_limits<double>::max()),
      upperParam(-std::numeric_limits<double>::max())
      {
        std::ifstream infile(fileName);

        std::string line;
        while (std::getline(infile, line))
        {
          std::regex  paramRegex("parameter=\"([^\"]*)\"");
          std::smatch paramMatch;
          std::regex  pvdRegex("file=\"([^\"]*)\"");
          std::smatch pvdMatch;

          // work around regex bug in GCC compiler
          const std::string lineCopy = line;
          bool paramFound = std::regex_search(lineCopy.begin(), lineCopy.end(), paramMatch, paramRegex);
          bool pvdFound   = std::regex_search(lineCopy.begin(), lineCopy.end(), pvdMatch,   pvdRegex);

          if (paramFound && pvdFound)
          {
            std::stringstream s;
            s << paramMatch[1];
            double param;
            s >> param;

            if (param > lastParam)
            {
              pvdReaders.push_back(std::pair<double,PVDReader>(param,PVDReader(pvdMatch[1],periodic,arrayId_,cutoffValue_)));
              lastParam = param;
              if (param < firstParam)
                firstParam = param;
            }
            else if (param < firstParam)
            {
              pvdReaders.push_front(std::pair<double,PVDReader>(param,PVDReader(pvdMatch[1],periodic,arrayId_,cutoffValue_)));
              firstParam = param;
            }
            else
            {
              typename std::list<std::pair<double,PVDReader> >::iterator it = pvdReaders.begin();
              while (it != pvdReaders.end() && it->first < param)
                ++it;
              pvdReaders.insert(it, std::pair<double,PVDReader>(param,PVDReader(pvdMatch[1],periodic,arrayId_,cutoffValue_)));
            }
          }
        }

        infile.close();
        if (pvdReaders.empty())
          DUNE_THROW(Dune::Exception,"ParamEnsemblePVDReader is empty, no PVD files found");

        lowerParam = firstParam;
        upperParam = firstParam;
        lowerData  = pvdReaders.begin();
        upperData  = pvdReaders.begin();
      }

    void setArrayId(int arrayId_)
    {
      std::list<std::pair<double,PVDReader> >::iterator it;
      for (it = pvdReaders.begin(); it != pvdReaders.end(); ++it)
        it->second.setArrayId(arrayId_);
    }

    int getArrayId() const
    {
      int arrayId = pvdReaders.begin()->second.getArrayId();
      std::list<std::pair<double,PVDReader> >::const_iterator it;
      for (it = pvdReaders.begin(); it != pvdReaders.end(); ++it)
        if (it->second.getArrayId() != arrayId)
          DUNE_THROW(Dune::Exception,"inconsistent arrayId in ParamEnsemblePVDReader");

      return arrayId;
    }

    int getNumberOfComponents() const
    {
      int numberOfComponents = pvdReaders.begin()->second.getNumberOfComponents();
      std::list<std::pair<double,PVDReader> >::const_iterator it;
      for (it = pvdReaders.begin(); it != pvdReaders.end(); ++it)
        if (it->second.getNumberOfComponents() != numberOfComponents)
          DUNE_THROW(Dune::Exception,"inconsistent numberOfComponents in PVDReader");

      return numberOfComponents;
    }

    template<typename Vector, typename Range, unsigned int dim>
      bool eval(const Vector& location, const Range& time, Vector& data) const
      {
        const Range param = location[dim-1];
        if (!updateIterators(param))
          return false;

        if (std::abs(param - lowerParam) < 1e-6)
        {
          data[dim-1] = lowerParam;
          return lowerData->second.eval<Vector,Range,dim-1>(location,time,data);
        }
        else if (std::abs(param - upperParam) < 1e-6)
        {
          data[dim-1] = upperParam;
          return upperData->second.eval<Vector,Range,dim-1>(location,time,data);
        }
        else
        {
          Vector lowerVector, upperVector;
          bool lowerValid, upperValid;
          lowerVector[dim-1] = lowerParam;
          upperVector[dim-1] = upperParam;
          lowerValid = lowerData->second.eval<Vector,Range,dim-1>(location,time,lowerVector);
          upperValid = upperData->second.eval<Vector,Range,dim-1>(location,time,upperVector);

          if (lowerValid && upperValid)
          {
            data = lowerVector;
            data *= (upperParam-param)/(upperParam-lowerParam);
            data.axpy((param-lowerParam)/(upperParam-lowerParam),upperVector);
            return true;
          }
          else
            return false;
        }
      }

    void report()
    {
      std::list<std::pair<double,PVDReader> >::const_iterator it;
      for (it = pvdReaders.begin(); it != pvdReaders.end(); ++it)
        it->second.report();
    }

  private:

    bool updateIterators(double param) const
    {
      if (param - firstParam < -1e-6 || param - lastParam > 1e-6)
        return false;

      if (param - firstParam < 1e-6)
      {
        lowerParam = firstParam;
        upperParam = firstParam;
        lowerData  = pvdReaders.begin();
        upperData  = pvdReaders.begin();
      }
      else if (!(param - upperParam < 1e-6) || !(lowerParam - param < 1e-6))
      {
        while (param - upperParam < -1e-6)
        {
          if (upperData == pvdReaders.begin())
            break;
          upperData--;
          upperParam = upperData->first;
        }
        while (param - upperParam > 1e-6)
        {
          upperData++;
          if (upperData == pvdReaders.end())
            return false;
          upperParam = upperData->first;
        }

        lowerData = upperData;
        lowerParam = lowerData->first;
        if (lowerParam - param > 1e-6)
        {
          if (lowerData == pvdReaders.begin())
            return false;
          lowerData--;
          lowerParam = lowerData->first;
        }
      }

      return true;
    }

};

#endif // DUNE_VISU_ENSEMBLEPVDREADER
