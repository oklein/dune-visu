# What is dune-visu?

dune-visu provides tools for the analysis of time-dependent
flow fields, namely FTLE and PCA computation.

# How to use dune-visu

dune-visu is written as a Dune module. You can put it as
a requirement into the *dune.module* file of your own module and
configure/build it through dunecontrol (see the documentation
of dune-common for details).

dune-visu requires dune-common for configuration and the
external library VTK to read in its vector field input.

Apart from dune-common, dune-visu has no dependencies on
other Dune modules, and can directly be used in other scientific
computing environments.

dune-visu is built by putting its code and that of
dune-common into two subdirectories of an arbitrary folder, e.g.
*$HOME/dune*, and then executing
"dune-common/bin/dunecontrol --opts=\<optsFile\> all",
where \<optsFile\> is an option file like this:

```bash
# subdirectory to use as build-directory
BUILDDIR="$HOME/dune/releaseBuild"
# paths to external software in non-default locations
CMAKE_PREFIX_PATH="$HOME/software"
# options that control compiler verbosity
GXX_WARNING_OPTS="-Wall -pedantic"
# options that control compiler behavior
GXX_OPTS="-march=native -g -O3 -std=c++14"
```

dune-visu generates a standalone binary that can be used to
read in flow fields and generate the corresponding FTLE and
PCA fields.

# Where to get help

If you have a problem with the use of dune-visu,
check the bug tracker at

https://gitlab.dune-project.org/oklein/dune-visu/issues

or contact the author directly:
* Ole Klein (ole.klein@iwr.uni-heidelberg.de)

# Acknowledgments

The initial implementation of this module, carried out by
Ole Klein, was part of the Transregional Collaborative Research Center
“Waves to Weather” (W2W; SFB/TRR165), funded by the DFG.
